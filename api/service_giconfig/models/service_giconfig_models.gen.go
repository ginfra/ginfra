// Package models provides primitives to interact with the openapi HTTP API.
//
// Code generated by gitlab.com/ginfra/ginfra version (devel) DO NOT EDIT.
package models

// Defines values for ConfigServiceSetRequestFailmode.
const (
	None     ConfigServiceSetRequestFailmode = "none"
	Rollback ConfigServiceSetRequestFailmode = "rollback"
)

// Defines values for GiConfigPortalParamsOperation.
const (
	Put    GiConfigPortalParamsOperation = "put"
	Remove GiConfigPortalParamsOperation = "remove"
)

// ConfigGetInitResponse defines model for ConfigGetInitResponse.
type ConfigGetInitResponse struct {
	// State Discovered uri
	State *int `json:"state,omitempty"`
}

// ConfigGetStateResponse defines model for ConfigGetStateResponse.
type ConfigGetStateResponse struct {
	Block *Base64 `json:"block,omitempty"`
}

// ConfigPath defines model for ConfigPath.
type ConfigPath = string

// ConfigPortalRequest defines model for ConfigPortalRequest.
type ConfigPortalRequest struct {
	// Portal Typically an address or DN for the portal.
	Portal *string `json:"portal,omitempty"`

	// Sclass Service class.
	Sclass *string `json:"sclass,omitempty"`
}

// ConfigQueryResponse defines model for ConfigQueryResponse.
type ConfigQueryResponse struct {
	Block *Base64     `json:"block,omitempty"`
	Qpath *ConfigPath `json:"qpath,omitempty"`
}

// ConfigServiceInstanceRequest defines model for ConfigServiceInstanceRequest.
type ConfigServiceInstanceRequest struct {
	// Deployment Deployment name.  If blank it will be the default deployment.
	Deployment *string `json:"deployment,omitempty"`

	// Fqdn FQDN for the entrypoint for the service.
	Fqdn *string `json:"fqdn,omitempty"`

	// Id IP address.
	Id *string `json:"id,omitempty"`

	// Ip IP address.
	Ip *string `json:"ip,omitempty"`

	// Itype Specific instance type.
	Itype *string `json:"itype,omitempty"`

	// Name System unique name
	Name *string `json:"name,omitempty"`

	// Sauth Service friendly config authorization token.
	Sauth *string `json:"sauth,omitempty"`
}

// ConfigServiceRegistrationRequest defines model for ConfigServiceRegistrationRequest.
type ConfigServiceRegistrationRequest struct {
	// Auth Service control authorization token.
	Auth *string `json:"auth,omitempty"`

	// Configprovider URI to configuration provider.  This is not authoritive.
	Configprovider *string `json:"configprovider,omitempty"`

	// Deployment Deployment name.  If blank it will be the default deployment.
	Deployment *string `json:"deployment,omitempty"`
	Image      *string `json:"image,omitempty"`

	// Itype Instance host type
	Itype *string `json:"itype,omitempty"`

	// Name System unique name
	Name *string `json:"name,omitempty"`

	// Port Exposed service port
	Port         *int32  `json:"port,omitempty"`
	Serviceclass *string `json:"serviceclass,omitempty"`
	Specifics    *Base64 `json:"specifics,omitempty"`
}

// ConfigServiceRegistrationResponse defines model for ConfigServiceRegistrationResponse.
type ConfigServiceRegistrationResponse struct {
	// Confauth A non-OAuth authorization token.
	Confauth *Authtoken `json:"confauth,omitempty"`

	// Name System unique name
	Name *string `json:"name,omitempty"`
}

// ConfigServiceSetRequest defines model for ConfigServiceSetRequest.
type ConfigServiceSetRequest struct {
	Failmode *ConfigServiceSetRequestFailmode `json:"failmode,omitempty"`
	Items    *[]ConfigSetItem                 `json:"items,omitempty"`
}

// ConfigServiceSetRequestFailmode defines model for ConfigServiceSetRequest.Failmode.
type ConfigServiceSetRequestFailmode string

// ConfigServiceSetStateRequest defines model for ConfigServiceSetStateRequest.
type ConfigServiceSetStateRequest struct {
	Block *Base64 `json:"block,omitempty"`
}

// ConfigSetItem defines model for ConfigSetItem.
type ConfigSetItem struct {
	Block *Base64     `json:"block,omitempty"`
	Path  *ConfigPath `json:"path,omitempty"`
}

// ConfigSetStateResponse defines model for ConfigSetStateResponse.
type ConfigSetStateResponse struct {
	// State State
	State *int `json:"state,omitempty"`
}

// ControlActionResponse defines model for ControlActionResponse.
type ControlActionResponse struct {
	Note *string `json:"note,omitempty"`
}

// ControlDatasourceInitRequest defines model for ControlDatasourceInitRequest.
type ControlDatasourceInitRequest struct {
	Name string `json:"name"`
}

// ControlDatasourceInitResponse defines model for ControlDatasourceInitResponse.
type ControlDatasourceInitResponse struct {
	Password string `json:"password"`
	Port     int    `json:"port"`
	Uri      string `json:"uri"`
	Username string `json:"username"`
}

// DiscoverPortalResponse defines model for DiscoverPortalResponse.
type DiscoverPortalResponse struct {
	// Uri Portal location
	Uri *string `json:"uri,omitempty"`
}

// DiscoverServiceResponse defines model for DiscoverServiceResponse.
type DiscoverServiceResponse struct {
	// Uri Discovered uri
	Uri *string `json:"uri,omitempty"`
}

// ErrorResponse defines model for ErrorResponse.
type ErrorResponse struct {
	Diagnostics *string `json:"diagnostics,omitempty"`
	Message     *string `json:"message,omitempty"`
}

// Specifics defines model for Specifics.
type Specifics = string

// Authtoken A non-OAuth authorization token.
type Authtoken = string

// Base64 defines model for base64.
type Base64 = string

// GiConfigDeregisterServiceParams defines parameters for GiConfigDeregisterService.
type GiConfigDeregisterServiceParams struct {
	// Confauth Administrative access token given to the config service when started.
	Confauth Authtoken `json:"confauth"`
}

// GiDiscoverPortalParams defines parameters for GiDiscoverPortal.
type GiDiscoverPortalParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiDiscoverServiceParams defines parameters for GiDiscoverService.
type GiDiscoverServiceParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigDiscoveryAddParams defines parameters for GiConfigDiscoveryAdd.
type GiConfigDiscoveryAddParams struct {
	// Confauth Administrative access token given to the config service when started.
	Confauth Authtoken `json:"confauth"`

	// Deployment Unique deployment name.
	Deployment string `json:"deployment"`

	// Sclass Service class to add.
	Sclass string `json:"sclass"`

	// Uri Uri for the service class.
	Uri string `json:"uri"`
}

// GiControlDatasourceInitParams defines parameters for GiControlDatasourceInit.
type GiControlDatasourceInitParams struct {
	// Auth Service auth access token generated when the service is registered and started.
	Auth *Authtoken `json:"auth,omitempty"`
}

// GiControlManageResetParams defines parameters for GiControlManageReset.
type GiControlManageResetParams struct {
	// Auth Service auth access token generated when the service is registered and started.
	Auth *Authtoken `json:"auth,omitempty"`
}

// GiControlManageStopParams defines parameters for GiControlManageStop.
type GiControlManageStopParams struct {
	// Auth Service auth access token generated when the service is registered and started.
	Auth *Authtoken `json:"auth,omitempty"`
}

// GiConfigRegisterInstanceParams defines parameters for GiConfigRegisterInstance.
type GiConfigRegisterInstanceParams struct {
	// Confauth Administrative access token given to the config service when started.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigPortalParams defines parameters for GiConfigPortal.
type GiConfigPortalParams struct {
	// Confauth Administrative access token given to the config service when started.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigPortalParamsOperation defines parameters for GiConfigPortal.
type GiConfigPortalParamsOperation string

// GiConfigQueryConfigParams defines parameters for GiConfigQueryConfig.
type GiConfigQueryConfigParams struct {
	// Qpath The dot separated path the configuration point.
	Qpath ConfigPath `json:"qpath"`

	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigRegisterServiceParams defines parameters for GiConfigRegisterService.
type GiConfigRegisterServiceParams struct {
	// Confauth Administrative access token given to the config service when started.
	Confauth Authtoken `json:"confauth"`
}

// GiRemoveConfigParams defines parameters for GiRemoveConfig.
type GiRemoveConfigParams struct {
	// Qpath The dot separated path the configuration point.
	Qpath ConfigPath `json:"qpath"`

	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigSetConfigParams defines parameters for GiConfigSetConfig.
type GiConfigSetConfigParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigGetStateParams defines parameters for GiConfigGetState.
type GiConfigGetStateParams struct {
	// Confauth Service access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigGetInitParams defines parameters for GiConfigGetInit.
type GiConfigGetInitParams struct {
	// Confauth Service access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigPostStateParams defines parameters for GiConfigPostState.
type GiConfigPostStateParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`
}

// GiConfigStoreGetParams defines parameters for GiConfigStoreGet.
type GiConfigStoreGetParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`

	// Deployment Unique deployment name or empty
	Deployment *string `json:"deployment,omitempty"`

	// Service Unique name of the service.  It may be empty.
	Service *string `json:"service,omitempty"`

	// Name File store name.
	Name string `json:"name"`

	// Raw Raw or processed.
	Raw bool `json:"raw"`
}

// GiConfigStorePutParams defines parameters for GiConfigStorePut.
type GiConfigStorePutParams struct {
	// Confauth Access token.
	Confauth Authtoken `json:"confauth"`

	// Deployment Unique deployment name or empty
	Deployment *string `json:"deployment,omitempty"`

	// Service Service name or empty
	Service *string `json:"service,omitempty"`

	// Name File store name.
	Name string `json:"name"`
}

// GiControlDatasourceInitJSONRequestBody defines body for GiControlDatasourceInit for application/json ContentType.
type GiControlDatasourceInitJSONRequestBody = ControlDatasourceInitRequest

// GiConfigRegisterInstanceJSONRequestBody defines body for GiConfigRegisterInstance for application/json ContentType.
type GiConfigRegisterInstanceJSONRequestBody = ConfigServiceInstanceRequest

// GiConfigPortalJSONRequestBody defines body for GiConfigPortal for application/json ContentType.
type GiConfigPortalJSONRequestBody = ConfigPortalRequest

// GiConfigRegisterServiceJSONRequestBody defines body for GiConfigRegisterService for application/json ContentType.
type GiConfigRegisterServiceJSONRequestBody = ConfigServiceRegistrationRequest

// GiConfigSetConfigJSONRequestBody defines body for GiConfigSetConfig for application/json ContentType.
type GiConfigSetConfigJSONRequestBody = ConfigServiceSetRequest

// GiConfigPostStateJSONRequestBody defines body for GiConfigPostState for application/json ContentType.
type GiConfigPostStateJSONRequestBody = ConfigServiceSetStateRequest
