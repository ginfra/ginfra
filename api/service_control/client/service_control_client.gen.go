// Package client provides primitives to interact with the openapi HTTP API.
//
// Code generated by gitlab.com/ginfra/ginfra version (devel) DO NOT EDIT.
package client

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/oapi-codegen/runtime"
	. "gitlab.com/ginfra/ginfra/api/service_control/models"
)

// RequestEditorFn  is the function signature for the RequestEditor callback function
type RequestEditorFn func(ctx context.Context, req *http.Request) error

// Doer performs HTTP requests.
//
// The standard http.Client implements this interface.
type HttpRequestDoer interface {
	Do(req *http.Request) (*http.Response, error)
}

// Client which conforms to the OpenAPI3 specification for this service.
type Client struct {
	// The endpoint of the server conforming to this interface, with scheme,
	// https://api.deepmap.com for example. This can contain a path relative
	// to the server, such as https://api.deepmap.com/dev-test, and all the
	// paths in the swagger spec will be appended to the server.
	Server string

	// Doer for performing requests, typically a *http.Client with any
	// customized settings, such as certificate chains.
	Client HttpRequestDoer

	// A list of callbacks for modifying requests which are generated before sending over
	// the network.
	RequestEditors []RequestEditorFn
}

// ClientOption allows setting custom parameters during construction
type ClientOption func(*Client) error

// Creates a new Client, with reasonable defaults
func NewClient(server string, opts ...ClientOption) (*Client, error) {
	// create a client with sane default values
	client := Client{
		Server: server,
	}
	// mutate client and add all optional params
	for _, o := range opts {
		if err := o(&client); err != nil {
			return nil, err
		}
	}
	// ensure the server URL always has a trailing slash
	if !strings.HasSuffix(client.Server, "/") {
		client.Server += "/"
	}
	// create httpClient, if not already present
	if client.Client == nil {
		client.Client = &http.Client{}
	}
	return &client, nil
}

// WithHTTPClient allows overriding the default Doer, which is
// automatically created using http.Client. This is useful for tests.
func WithHTTPClient(doer HttpRequestDoer) ClientOption {
	return func(c *Client) error {
		c.Client = doer
		return nil
	}
}

// WithRequestEditorFn allows setting up a callback function, which will be
// called right before sending the request. This can be used to mutate the request.
func WithRequestEditorFn(fn RequestEditorFn) ClientOption {
	return func(c *Client) error {
		c.RequestEditors = append(c.RequestEditors, fn)
		return nil
	}
}

// The interface specification for the client above.
type ClientInterface interface {
	// GiControlDatasourceInitWithBody request with any body
	GiControlDatasourceInitWithBody(ctx context.Context, params *GiControlDatasourceInitParams, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error)

	GiControlDatasourceInit(ctx context.Context, params *GiControlDatasourceInitParams, body GiControlDatasourceInitJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error)

	// GiControlManageReset request
	GiControlManageReset(ctx context.Context, params *GiControlManageResetParams, reqEditors ...RequestEditorFn) (*http.Response, error)

	// GiControlManageStop request
	GiControlManageStop(ctx context.Context, params *GiControlManageStopParams, reqEditors ...RequestEditorFn) (*http.Response, error)

	// GiGetSpecifics request
	GiGetSpecifics(ctx context.Context, reqEditors ...RequestEditorFn) (*http.Response, error)
}

func (c *Client) GiControlDatasourceInitWithBody(ctx context.Context, params *GiControlDatasourceInitParams, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGiControlDatasourceInitRequestWithBody(c.Server, params, contentType, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GiControlDatasourceInit(ctx context.Context, params *GiControlDatasourceInitParams, body GiControlDatasourceInitJSONRequestBody, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGiControlDatasourceInitRequest(c.Server, params, body)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GiControlManageReset(ctx context.Context, params *GiControlManageResetParams, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGiControlManageResetRequest(c.Server, params)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GiControlManageStop(ctx context.Context, params *GiControlManageStopParams, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGiControlManageStopRequest(c.Server, params)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

func (c *Client) GiGetSpecifics(ctx context.Context, reqEditors ...RequestEditorFn) (*http.Response, error) {
	req, err := NewGiGetSpecificsRequest(c.Server)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	if err := c.applyEditors(ctx, req, reqEditors); err != nil {
		return nil, err
	}
	return c.Client.Do(req)
}

// NewGiControlDatasourceInitRequest calls the generic GiControlDatasourceInit builder with application/json body
func NewGiControlDatasourceInitRequest(server string, params *GiControlDatasourceInitParams, body GiControlDatasourceInitJSONRequestBody) (*http.Request, error) {
	var bodyReader io.Reader
	buf, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	bodyReader = bytes.NewReader(buf)
	return NewGiControlDatasourceInitRequestWithBody(server, params, "application/json", bodyReader)
}

// NewGiControlDatasourceInitRequestWithBody generates requests for GiControlDatasourceInit with any type of body
func NewGiControlDatasourceInitRequestWithBody(server string, params *GiControlDatasourceInitParams, contentType string, body io.Reader) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/ginfra/datasource/init")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", queryURL.String(), body)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", contentType)

	if params != nil {

		if params.Auth != nil {
			var headerParam0 string

			headerParam0, err = runtime.StyleParamWithLocation("simple", false, "auth", runtime.ParamLocationHeader, *params.Auth)
			if err != nil {
				return nil, err
			}

			req.Header.Set("auth", headerParam0)
		}

	}

	return req, nil
}

// NewGiControlManageResetRequest generates requests for GiControlManageReset
func NewGiControlManageResetRequest(server string, params *GiControlManageResetParams) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/ginfra/manage/reset")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	if params != nil {

		if params.Auth != nil {
			var headerParam0 string

			headerParam0, err = runtime.StyleParamWithLocation("simple", false, "auth", runtime.ParamLocationHeader, *params.Auth)
			if err != nil {
				return nil, err
			}

			req.Header.Set("auth", headerParam0)
		}

	}

	return req, nil
}

// NewGiControlManageStopRequest generates requests for GiControlManageStop
func NewGiControlManageStopRequest(server string, params *GiControlManageStopParams) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/ginfra/manage/stop")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	if params != nil {

		if params.Auth != nil {
			var headerParam0 string

			headerParam0, err = runtime.StyleParamWithLocation("simple", false, "auth", runtime.ParamLocationHeader, *params.Auth)
			if err != nil {
				return nil, err
			}

			req.Header.Set("auth", headerParam0)
		}

	}

	return req, nil
}

// NewGiGetSpecificsRequest generates requests for GiGetSpecifics
func NewGiGetSpecificsRequest(server string) (*http.Request, error) {
	var err error

	serverURL, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	operationPath := fmt.Sprintf("/ginfra/specifics")
	if operationPath[0] == '/' {
		operationPath = "." + operationPath
	}

	queryURL, err := serverURL.Parse(operationPath)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("GET", queryURL.String(), nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func (c *Client) applyEditors(ctx context.Context, req *http.Request, additionalEditors []RequestEditorFn) error {
	for _, r := range c.RequestEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	for _, r := range additionalEditors {
		if err := r(ctx, req); err != nil {
			return err
		}
	}
	return nil
}

// ClientWithResponses builds on ClientInterface to offer response payloads
type ClientWithResponses struct {
	ClientInterface
}

// NewClientWithResponses creates a new ClientWithResponses, which wraps
// Client with return type handling
func NewClientWithResponses(server string, opts ...ClientOption) (*ClientWithResponses, error) {
	client, err := NewClient(server, opts...)
	if err != nil {
		return nil, err
	}
	return &ClientWithResponses{client}, nil
}

// WithBaseURL overrides the baseURL.
func WithBaseURL(baseURL string) ClientOption {
	return func(c *Client) error {
		newBaseURL, err := url.Parse(baseURL)
		if err != nil {
			return err
		}
		c.Server = newBaseURL.String()
		return nil
	}
}

// ClientWithResponsesInterface is the interface specification for the client with responses above.
type ClientWithResponsesInterface interface {
	// GiControlDatasourceInitWithBodyWithResponse request with any body
	GiControlDatasourceInitWithBodyWithResponse(ctx context.Context, params *GiControlDatasourceInitParams, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*GiControlDatasourceInitResponse, error)

	GiControlDatasourceInitWithResponse(ctx context.Context, params *GiControlDatasourceInitParams, body GiControlDatasourceInitJSONRequestBody, reqEditors ...RequestEditorFn) (*GiControlDatasourceInitResponse, error)

	// GiControlManageResetWithResponse request
	GiControlManageResetWithResponse(ctx context.Context, params *GiControlManageResetParams, reqEditors ...RequestEditorFn) (*GiControlManageResetResponse, error)

	// GiControlManageStopWithResponse request
	GiControlManageStopWithResponse(ctx context.Context, params *GiControlManageStopParams, reqEditors ...RequestEditorFn) (*GiControlManageStopResponse, error)

	// GiGetSpecificsWithResponse request
	GiGetSpecificsWithResponse(ctx context.Context, reqEditors ...RequestEditorFn) (*GiGetSpecificsResponse, error)
}

type GiControlDatasourceInitResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *ControlDatasourceInitResponse
	JSON400      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r GiControlDatasourceInitResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GiControlDatasourceInitResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type GiControlManageResetResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *ControlActionResponse
	JSON400      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r GiControlManageResetResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GiControlManageResetResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type GiControlManageStopResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *ControlActionResponse
	JSON400      *ErrorResponse
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r GiControlManageStopResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GiControlManageStopResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

type GiGetSpecificsResponse struct {
	Body         []byte
	HTTPResponse *http.Response
	JSON200      *Specifics
	JSON500      *ErrorResponse
}

// Status returns HTTPResponse.Status
func (r GiGetSpecificsResponse) Status() string {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.Status
	}
	return http.StatusText(0)
}

// StatusCode returns HTTPResponse.StatusCode
func (r GiGetSpecificsResponse) StatusCode() int {
	if r.HTTPResponse != nil {
		return r.HTTPResponse.StatusCode
	}
	return 0
}

// GiControlDatasourceInitWithBodyWithResponse request with arbitrary body returning *GiControlDatasourceInitResponse
func (c *ClientWithResponses) GiControlDatasourceInitWithBodyWithResponse(ctx context.Context, params *GiControlDatasourceInitParams, contentType string, body io.Reader, reqEditors ...RequestEditorFn) (*GiControlDatasourceInitResponse, error) {
	rsp, err := c.GiControlDatasourceInitWithBody(ctx, params, contentType, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGiControlDatasourceInitResponse(rsp)
}

func (c *ClientWithResponses) GiControlDatasourceInitWithResponse(ctx context.Context, params *GiControlDatasourceInitParams, body GiControlDatasourceInitJSONRequestBody, reqEditors ...RequestEditorFn) (*GiControlDatasourceInitResponse, error) {
	rsp, err := c.GiControlDatasourceInit(ctx, params, body, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGiControlDatasourceInitResponse(rsp)
}

// GiControlManageResetWithResponse request returning *GiControlManageResetResponse
func (c *ClientWithResponses) GiControlManageResetWithResponse(ctx context.Context, params *GiControlManageResetParams, reqEditors ...RequestEditorFn) (*GiControlManageResetResponse, error) {
	rsp, err := c.GiControlManageReset(ctx, params, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGiControlManageResetResponse(rsp)
}

// GiControlManageStopWithResponse request returning *GiControlManageStopResponse
func (c *ClientWithResponses) GiControlManageStopWithResponse(ctx context.Context, params *GiControlManageStopParams, reqEditors ...RequestEditorFn) (*GiControlManageStopResponse, error) {
	rsp, err := c.GiControlManageStop(ctx, params, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGiControlManageStopResponse(rsp)
}

// GiGetSpecificsWithResponse request returning *GiGetSpecificsResponse
func (c *ClientWithResponses) GiGetSpecificsWithResponse(ctx context.Context, reqEditors ...RequestEditorFn) (*GiGetSpecificsResponse, error) {
	rsp, err := c.GiGetSpecifics(ctx, reqEditors...)
	if err != nil {
		return nil, err
	}
	return ParseGiGetSpecificsResponse(rsp)
}

// ParseGiControlDatasourceInitResponse parses an HTTP response from a GiControlDatasourceInitWithResponse call
func ParseGiControlDatasourceInitResponse(rsp *http.Response) (*GiControlDatasourceInitResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GiControlDatasourceInitResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest ControlDatasourceInitResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseGiControlManageResetResponse parses an HTTP response from a GiControlManageResetWithResponse call
func ParseGiControlManageResetResponse(rsp *http.Response) (*GiControlManageResetResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GiControlManageResetResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest ControlActionResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseGiControlManageStopResponse parses an HTTP response from a GiControlManageStopWithResponse call
func ParseGiControlManageStopResponse(rsp *http.Response) (*GiControlManageStopResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GiControlManageStopResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest ControlActionResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 400:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON400 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// ParseGiGetSpecificsResponse parses an HTTP response from a GiGetSpecificsWithResponse call
func ParseGiGetSpecificsResponse(rsp *http.Response) (*GiGetSpecificsResponse, error) {
	bodyBytes, err := io.ReadAll(rsp.Body)
	defer func() { _ = rsp.Body.Close() }()
	if err != nil {
		return nil, err
	}

	response := &GiGetSpecificsResponse{
		Body:         bodyBytes,
		HTTPResponse: rsp,
	}

	switch {
	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 200:
		var dest Specifics
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON200 = &dest

	case strings.Contains(rsp.Header.Get("Content-Type"), "json") && rsp.StatusCode == 500:
		var dest ErrorResponse
		if err := json.Unmarshal(bodyBytes, &dest); err != nil {
			return nil, err
		}
		response.JSON500 = &dest

	}

	return response, nil
}

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+xXa1PjNhT9K1pBpzY4sRMIXdzZMWwfTGZK24H91CjLqPbF0daWvNI1LI/0t3ckGwIb",
	"85jZwuzs8AWIHvdcHZ1zRC5oqspKSZBoaHxBTTqDkrs/f1IStSp2UxRKHoCplDRgJyqtKtAowC2TCptR",
	"jgha0pi+9ya8d77b+2u6vurTgOJZBTSmBrWQOZ3Pr0fU3x8gRToPrrB+5siNqnUKYynwAD7WYLADkpd3",
	"QQ6GG5ujrR9eb0d3gmv4WAsNGY0nTaXp4xu6i4SKG3OqdPZgV4z1GdthbJWxFcZeMfaesTXGPMb87oYD",
	"WintOCj5J1HWJY23RqONUUBLIZvPgyi63iYkQg7a7qu16Gpnan9Eve31fm+65sdeEjMWMhZ6XhJ7yRv7",
	"q13R6x/9+2qV1VE03Pre89fWgx/fxNPL7yZRb3u39+v0Yjj313zfY2zD3/ETu5mxSTsb96cXw2DO2PTy",
	"/orLBRkbeUkcN/UyN7DlJxYnfESDO4yFXTVf+8mlLZF4ySvGQv8LSg0i244lLnlklaSzzMCSFq98UY2h",
	"n6yGoks3tQF9j1Me5Y/rGsFC4q0iG4F1eecXrZW+2yuZ4LlUBkXqPi71XYIxPIeOua7kOKwgFcd31eI1",
	"zlD9A9IBg0m1qGyc0ZjuEqlk74/dGmfELlNanHM7R9yGvjvyFWvexDLmr3cQNg+okMdqGeAQ9AlokjZJ",
	"QgzoE5GCrVuIFFpqmgui++N35Ld21BJb0JjOECsTh6GqQDYZ1Fc6D9vNJtwfv7NHRIGFa8jBHbVwRy0c",
	"DegJaNN0NOhHNKCfeoXKXb8NjuPVgvBK0Jhu9KP+RnP4mSM1zIU81jzMrrMwFFI0sayaeL598DESYUhd",
	"EVQEZ0BEWRVQgsSGXiXJTJ3aydoAwZkwlhOrEDc/zmhM90RnALu2NC8BQRsaT7ooFym4+yQ8TcGY5jZJ",
	"DtLWh4yczkC6tlqGbK8acmEQNGSEy4wY5Bohs20JW3YGPANNg6vbsuVp0L6V9vyrGo5pTFfCxWMati9p",
	"uJDgfD5t3AUG36rszO601wXSkcirqhCp4yD8YJRcPMcPQdz7ejqFfnZBUqCjwF4pabVFbzofdQ0uChoT",
	"Ox0Mo+ipO24jo6Plw9rdphX85v/Yx+2o6sB9yzOir5i02INlvf+u8DpBIGuWbS4v+5NrFGld8EUmSIUL",
	"d0BGjpV2hiCNmfu21ug5jzuWNvB44Ra6YDB1WXJ91qpG8EKcA+G3lWPXXaVEySXPIdRg4JkiYt8hHjjA",
	"byEfntZyn/07/2K19prDr9FyTtQ3xbjoctlyBlX1nI47tHgvhnsx3DfwtlktP+gzc/OrRg4dJtsDJItV",
	"y/7ZAzy8Mf1k0luA3C+3r4b/W8QtZbOtNv8vAAD//7B2mrmqEgAA",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %w", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	res := make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	resolvePath := PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		pathToFile := url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
