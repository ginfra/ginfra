// Package server provides primitives to interact with the openapi HTTP API.
//
// Code generated by gitlab.com/ginfra/ginfra version (devel) DO NOT EDIT.
package server

import (
	"bytes"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strings"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/labstack/echo/v4"
	"github.com/oapi-codegen/runtime"
	strictecho "github.com/oapi-codegen/runtime/strictmiddleware/echo"
	. "gitlab.com/ginfra/gservices/web_userview/api/control/models"
)

// ServerInterface represents all server handlers.
type ServerInterface interface {
	// Initialize a data source.
	// (POST /web_userview/ginfra/datasource/init)
	GiControlDatasourceInit(ctx echo.Context) error
	// Reset the service/server.
	// (POST /web_userview/ginfra/manage/reset)
	GiControlManageReset(ctx echo.Context, params GiControlManageResetParams) error
	// Stop the service/server.
	// (POST /web_userview/ginfra/manage/stop)
	GiControlManageStop(ctx echo.Context, params GiControlManageStopParams) error
	// Get specifics for this service
	// (GET /web_userview/ginfra/specifics)
	GiGetSpecifics(ctx echo.Context) error
}

// ServerInterfaceWrapper converts echo contexts to parameters.
type ServerInterfaceWrapper struct {
	Handler ServerInterface
}

// GiControlDatasourceInit converts echo context to params.
func (w *ServerInterfaceWrapper) GiControlDatasourceInit(ctx echo.Context) error {
	var err error

	// Invoke the callback with all the unmarshaled arguments
	err = w.Handler.GiControlDatasourceInit(ctx)
	return err
}

// GiControlManageReset converts echo context to params.
func (w *ServerInterfaceWrapper) GiControlManageReset(ctx echo.Context) error {
	var err error

	// Parameter object where we will unmarshal all parameters from the context
	var params GiControlManageResetParams

	headers := ctx.Request().Header
	// ------------- Optional header parameter "auth" -------------
	if valueList, found := headers[http.CanonicalHeaderKey("auth")]; found {
		var Auth Authtoken
		n := len(valueList)
		if n != 1 {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Expected one value for auth, got %d", n))
		}

		err = runtime.BindStyledParameterWithOptions("simple", "auth", valueList[0], &Auth, runtime.BindStyledParameterOptions{ParamLocation: runtime.ParamLocationHeader, Explode: false, Required: false})
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for parameter auth: %s", err))
		}

		params.Auth = &Auth
	}

	// Invoke the callback with all the unmarshaled arguments
	err = w.Handler.GiControlManageReset(ctx, params)
	return err
}

// GiControlManageStop converts echo context to params.
func (w *ServerInterfaceWrapper) GiControlManageStop(ctx echo.Context) error {
	var err error

	// Parameter object where we will unmarshal all parameters from the context
	var params GiControlManageStopParams

	headers := ctx.Request().Header
	// ------------- Optional header parameter "auth" -------------
	if valueList, found := headers[http.CanonicalHeaderKey("auth")]; found {
		var Auth Authtoken
		n := len(valueList)
		if n != 1 {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Expected one value for auth, got %d", n))
		}

		err = runtime.BindStyledParameterWithOptions("simple", "auth", valueList[0], &Auth, runtime.BindStyledParameterOptions{ParamLocation: runtime.ParamLocationHeader, Explode: false, Required: false})
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Invalid format for parameter auth: %s", err))
		}

		params.Auth = &Auth
	}

	// Invoke the callback with all the unmarshaled arguments
	err = w.Handler.GiControlManageStop(ctx, params)
	return err
}

// GiGetSpecifics converts echo context to params.
func (w *ServerInterfaceWrapper) GiGetSpecifics(ctx echo.Context) error {
	var err error

	// Invoke the callback with all the unmarshaled arguments
	err = w.Handler.GiGetSpecifics(ctx)
	return err
}

// This is a simple interface which specifies echo.Route addition functions which
// are present on both echo.Echo and echo.Group, since we want to allow using
// either of them for path registration
type EchoRouter interface {
	CONNECT(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	DELETE(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	GET(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	HEAD(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	OPTIONS(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	PATCH(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	POST(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	PUT(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
	TRACE(path string, h echo.HandlerFunc, m ...echo.MiddlewareFunc) *echo.Route
}

// RegisterHandlers adds each server route to the EchoRouter.
func RegisterHandlers(router EchoRouter, si ServerInterface) {
	RegisterHandlersWithBaseURL(router, si, "")
}

// Registers handlers, and prepends BaseURL to the paths, so that the paths
// can be served under a prefix.
func RegisterHandlersWithBaseURL(router EchoRouter, si ServerInterface, baseURL string) {

	wrapper := ServerInterfaceWrapper{
		Handler: si,
	}

	router.POST(baseURL+"/web_userview/ginfra/datasource/init", wrapper.GiControlDatasourceInit)
	router.POST(baseURL+"/web_userview/ginfra/manage/reset", wrapper.GiControlManageReset)
	router.POST(baseURL+"/web_userview/ginfra/manage/stop", wrapper.GiControlManageStop)
	router.GET(baseURL+"/web_userview/ginfra/specifics", wrapper.GiGetSpecifics)

}

type GiControlDatasourceInitRequestObject struct {
	Body *GiControlDatasourceInitJSONRequestBody
}

type GiControlDatasourceInitResponseObject interface {
	VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error
}

type GiControlDatasourceInit200JSONResponse ControlDatasourceInitResponse

func (response GiControlDatasourceInit200JSONResponse) VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response)
}

type GiControlDatasourceInit400JSONResponse ErrorResponse

func (response GiControlDatasourceInit400JSONResponse) VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)

	return json.NewEncoder(w).Encode(response)
}

type GiControlDatasourceInit401Response struct {
}

func (response GiControlDatasourceInit401Response) VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error {
	w.WriteHeader(401)
	return nil
}

type GiControlDatasourceInit404Response struct {
}

func (response GiControlDatasourceInit404Response) VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error {
	w.WriteHeader(404)
	return nil
}

type GiControlDatasourceInit500JSONResponse ErrorResponse

func (response GiControlDatasourceInit500JSONResponse) VisitGiControlDatasourceInitResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageResetRequestObject struct {
	Params GiControlManageResetParams
}

type GiControlManageResetResponseObject interface {
	VisitGiControlManageResetResponse(w http.ResponseWriter) error
}

type GiControlManageReset200JSONResponse ControlActionResponse

func (response GiControlManageReset200JSONResponse) VisitGiControlManageResetResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageReset400JSONResponse ErrorResponse

func (response GiControlManageReset400JSONResponse) VisitGiControlManageResetResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageReset401Response struct {
}

func (response GiControlManageReset401Response) VisitGiControlManageResetResponse(w http.ResponseWriter) error {
	w.WriteHeader(401)
	return nil
}

type GiControlManageReset404Response struct {
}

func (response GiControlManageReset404Response) VisitGiControlManageResetResponse(w http.ResponseWriter) error {
	w.WriteHeader(404)
	return nil
}

type GiControlManageReset500JSONResponse ErrorResponse

func (response GiControlManageReset500JSONResponse) VisitGiControlManageResetResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageStopRequestObject struct {
	Params GiControlManageStopParams
}

type GiControlManageStopResponseObject interface {
	VisitGiControlManageStopResponse(w http.ResponseWriter) error
}

type GiControlManageStop200JSONResponse ControlActionResponse

func (response GiControlManageStop200JSONResponse) VisitGiControlManageStopResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageStop400JSONResponse ErrorResponse

func (response GiControlManageStop400JSONResponse) VisitGiControlManageStopResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)

	return json.NewEncoder(w).Encode(response)
}

type GiControlManageStop401Response struct {
}

func (response GiControlManageStop401Response) VisitGiControlManageStopResponse(w http.ResponseWriter) error {
	w.WriteHeader(401)
	return nil
}

type GiControlManageStop404Response struct {
}

func (response GiControlManageStop404Response) VisitGiControlManageStopResponse(w http.ResponseWriter) error {
	w.WriteHeader(404)
	return nil
}

type GiControlManageStop500JSONResponse ErrorResponse

func (response GiControlManageStop500JSONResponse) VisitGiControlManageStopResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)

	return json.NewEncoder(w).Encode(response)
}

type GiGetSpecificsRequestObject struct {
}

type GiGetSpecificsResponseObject interface {
	VisitGiGetSpecificsResponse(w http.ResponseWriter) error
}

type GiGetSpecifics200JSONResponse Specifics

func (response GiGetSpecifics200JSONResponse) VisitGiGetSpecificsResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	return json.NewEncoder(w).Encode(response)
}

type GiGetSpecifics500JSONResponse ErrorResponse

func (response GiGetSpecifics500JSONResponse) VisitGiGetSpecificsResponse(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)

	return json.NewEncoder(w).Encode(response)
}

// StrictServerInterface represents all server handlers.
type StrictServerInterface interface {
	// Initialize a data source.
	// (POST /web_userview/ginfra/datasource/init)
	GiControlDatasourceInit(ctx context.Context, request GiControlDatasourceInitRequestObject) (GiControlDatasourceInitResponseObject, error)
	// Reset the service/server.
	// (POST /web_userview/ginfra/manage/reset)
	GiControlManageReset(ctx context.Context, request GiControlManageResetRequestObject) (GiControlManageResetResponseObject, error)
	// Stop the service/server.
	// (POST /web_userview/ginfra/manage/stop)
	GiControlManageStop(ctx context.Context, request GiControlManageStopRequestObject) (GiControlManageStopResponseObject, error)
	// Get specifics for this service
	// (GET /web_userview/ginfra/specifics)
	GiGetSpecifics(ctx context.Context, request GiGetSpecificsRequestObject) (GiGetSpecificsResponseObject, error)
}

type StrictHandlerFunc = strictecho.StrictEchoHandlerFunc
type StrictMiddlewareFunc = strictecho.StrictEchoMiddlewareFunc

func NewStrictHandler(ssi StrictServerInterface, middlewares []StrictMiddlewareFunc) ServerInterface {
	return &strictHandler{ssi: ssi, middlewares: middlewares}
}

type strictHandler struct {
	ssi         StrictServerInterface
	middlewares []StrictMiddlewareFunc
}

// GiControlDatasourceInit operation middleware
func (sh *strictHandler) GiControlDatasourceInit(ctx echo.Context) error {
	var request GiControlDatasourceInitRequestObject

	var body GiControlDatasourceInitJSONRequestBody
	if err := ctx.Bind(&body); err != nil {
		return err
	}
	request.Body = &body

	handler := func(ctx echo.Context, request interface{}) (interface{}, error) {
		return sh.ssi.GiControlDatasourceInit(ctx.Request().Context(), request.(GiControlDatasourceInitRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "GiControlDatasourceInit")
	}

	response, err := handler(ctx, request)

	if err != nil {
		return err
	} else if validResponse, ok := response.(GiControlDatasourceInitResponseObject); ok {
		return validResponse.VisitGiControlDatasourceInitResponse(ctx.Response())
	} else if response != nil {
		return fmt.Errorf("unexpected response type: %T", response)
	}
	return nil
}

// GiControlManageReset operation middleware
func (sh *strictHandler) GiControlManageReset(ctx echo.Context, params GiControlManageResetParams) error {
	var request GiControlManageResetRequestObject

	request.Params = params

	handler := func(ctx echo.Context, request interface{}) (interface{}, error) {
		return sh.ssi.GiControlManageReset(ctx.Request().Context(), request.(GiControlManageResetRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "GiControlManageReset")
	}

	response, err := handler(ctx, request)

	if err != nil {
		return err
	} else if validResponse, ok := response.(GiControlManageResetResponseObject); ok {
		return validResponse.VisitGiControlManageResetResponse(ctx.Response())
	} else if response != nil {
		return fmt.Errorf("unexpected response type: %T", response)
	}
	return nil
}

// GiControlManageStop operation middleware
func (sh *strictHandler) GiControlManageStop(ctx echo.Context, params GiControlManageStopParams) error {
	var request GiControlManageStopRequestObject

	request.Params = params

	handler := func(ctx echo.Context, request interface{}) (interface{}, error) {
		return sh.ssi.GiControlManageStop(ctx.Request().Context(), request.(GiControlManageStopRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "GiControlManageStop")
	}

	response, err := handler(ctx, request)

	if err != nil {
		return err
	} else if validResponse, ok := response.(GiControlManageStopResponseObject); ok {
		return validResponse.VisitGiControlManageStopResponse(ctx.Response())
	} else if response != nil {
		return fmt.Errorf("unexpected response type: %T", response)
	}
	return nil
}

// GiGetSpecifics operation middleware
func (sh *strictHandler) GiGetSpecifics(ctx echo.Context) error {
	var request GiGetSpecificsRequestObject

	handler := func(ctx echo.Context, request interface{}) (interface{}, error) {
		return sh.ssi.GiGetSpecifics(ctx.Request().Context(), request.(GiGetSpecificsRequestObject))
	}
	for _, middleware := range sh.middlewares {
		handler = middleware(handler, "GiGetSpecifics")
	}

	response, err := handler(ctx, request)

	if err != nil {
		return err
	} else if validResponse, ok := response.(GiGetSpecificsResponseObject); ok {
		return validResponse.VisitGiGetSpecificsResponse(ctx.Response())
	} else if response != nil {
		return fmt.Errorf("unexpected response type: %T", response)
	}
	return nil
}

// Base64 encoded, gzipped, json marshaled Swagger object
var swaggerSpec = []string{

	"H4sIAAAAAAAC/+xWbVPjNhf9K0LwzGODEzuB0MWdHcP2hclMaTuwnxplGa19cbS1Ja90DctL+ts7kg2B",
	"jWHpbGF2OnyBRC/nXB2dc5VLmqqyUhIkGhpfUpPOoOTu4w9KolbFXopCyUMwlZIG7ESlVQUaBbhlUmEz",
	"yhFBSxrTd96E9y72en9MN9Z8GlA8r4DG1KAWMqfz+c2Iev8BUqTz4JrrR47cqFqnMJYCD+FjDQY7KHl5",
	"H+VguLk12v7u1U50L7mGj7XQkNF40iBNH1/QfSJU3JgzpbMvVsVYn7FdxtYYW2VshbF3jK0z5jHmdxcc",
	"0Eppp0HJP4myLmm8PRptjgJaCtl8H0TRzTYhEXLQdl+tRVc5U/sn6u1s9HvTdT/2kpixkLHQ85LYS17b",
	"f+2KXv/4r5U1VkfRcPv/nr++EXz/Op5e/W8S9Xb2ej9PL4dzf933PcY2/V0/sZsZm7SzcX96OQzmjE2v",
	"HkZcBmRs5CVx3OBlbmDbTyxP+IgCdxkLuzBf+cmVhUi8ZIWx0P8KqEFky7HCJY9ESTphBla0ePWrMIZ+",
	"shaKLt/UBvQDSXlUPm4wgoXFW0c2BuvKzk9aK31/VjLBc6kMitR9Xaq7BGN4Dh1zXZ3jqIJUnNyHxWuc",
	"ofoTpCMGk2pR2XZGY7pHpJK93/ZqnBG7TGlxwe0ccRv67sjXqnkTq5i/0SHYPKBCnqhlgiPQp6BJ2nQS",
	"YkCfihQsbiFSaKVpLogejN+SX9pRK2xBYzpDrEwchqoC2fSgvtJ52G424cH4rT0iCixcQY7uuKU7bulo",
	"QE9Bm6aiQT+iAf3UK1Tu6m14nK6WhFeCxnSzH/U3m8PPnKjhGbw/rh0enIW5kCeah9lNYwyFFE2PVk2v",
	"vqvCGIkwpK4IKoIzIKKsCihBYqO1kmSmzuxkbYDgTBgrkLWLmx9nNKb7orMb08aqYPCNys4tsz07SFcE",
	"r6pCpA4j/GCUXLxt9tOahhMa09Vw8fiF7csXPvgUuev+7IBSoDuZlYS0F0Vvxwh1DS5XTSKcqMMoeuqK",
	"2/x1lHxUpykYY92z9S/WcTf3HbxveEb0tZKWe7Dsl18V3sQRsmbZ1vKy37lGkdYFXwRMKly4CzJyorQz",
	"FGmS0bdYo+c87lja7sELt9ClzNRlyfV56xrBC3EBhN91jl3XGbmSS55DqMHAM+XtwDEeOkLbEDQvAUEb",
	"Gk+6mp1IwV0d4c5eTR8lOUgLDhk5m4F0NbW9yRaqIRcGQUNGuMyIQa4RMluTsLAz4BloGlz3SQtPg0de",
	"0KL5z+fTp8/fZz+UX3LXXnP4LebPmfq2GRdVfiF/BlX1nPE7snwv6XtJ33/g1bNe/mehM7d/3ufQkbh9",
	"QLJYtRymfcCjW9NP5sMFycPe+2Yu445wS13bos3/DgAA//9bJmPeHhIAAA==",
}

// GetSwagger returns the content of the embedded swagger specification file
// or error if failed to decode
func decodeSpec() ([]byte, error) {
	zipped, err := base64.StdEncoding.DecodeString(strings.Join(swaggerSpec, ""))
	if err != nil {
		return nil, fmt.Errorf("error base64 decoding spec: %w", err)
	}
	zr, err := gzip.NewReader(bytes.NewReader(zipped))
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}
	var buf bytes.Buffer
	_, err = buf.ReadFrom(zr)
	if err != nil {
		return nil, fmt.Errorf("error decompressing spec: %w", err)
	}

	return buf.Bytes(), nil
}

var rawSpec = decodeSpecCached()

// a naive cached of a decoded swagger spec
func decodeSpecCached() func() ([]byte, error) {
	data, err := decodeSpec()
	return func() ([]byte, error) {
		return data, err
	}
}

// Constructs a synthetic filesystem for resolving external references when loading openapi specifications.
func PathToRawSpec(pathToFile string) map[string]func() ([]byte, error) {
	res := make(map[string]func() ([]byte, error))
	if len(pathToFile) > 0 {
		res[pathToFile] = rawSpec
	}

	return res
}

// GetSwagger returns the Swagger specification corresponding to the generated code
// in this file. The external references of Swagger specification are resolved.
// The logic of resolving external references is tightly connected to "import-mapping" feature.
// Externally referenced files must be embedded in the corresponding golang packages.
// Urls can be supported but this task was out of the scope.
func GetSwagger() (swagger *openapi3.T, err error) {
	resolvePath := PathToRawSpec("")

	loader := openapi3.NewLoader()
	loader.IsExternalRefsAllowed = true
	loader.ReadFromURIFunc = func(loader *openapi3.Loader, url *url.URL) ([]byte, error) {
		pathToFile := url.String()
		pathToFile = path.Clean(pathToFile)
		getSpec, ok := resolvePath[pathToFile]
		if !ok {
			err1 := fmt.Errorf("path not found: %s", pathToFile)
			return nil, err1
		}
		return getSpec()
	}
	var specData []byte
	specData, err = rawSpec()
	if err != nil {
		return
	}
	swagger, err = loader.LoadFromData(specData)
	if err != nil {
		return
	}
	return
}
