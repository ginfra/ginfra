/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { base64 } from './base64';

export type ConfigGetStateResponse = {
    /**
     * Base64 json block representing the config.
     */
    block?: base64;
};
