#!/usr/bin/env bash

##########################################################################
# Place custom entry provision tasks performed during docker build here
# This file will not be updated, so any additions or changes are safe.


echo "Custom provisioning unique to this service."
