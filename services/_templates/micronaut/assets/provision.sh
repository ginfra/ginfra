#!/usr/bin/env bash

########################################################################################
# DO NOT EDIT THIS FILE
# Put custom provisioning tasks in the file custom_provision.sh


mkdir -p /service/log

apt update -y
apt install -y wget

wget https://download.oracle.com/java/21/latest/jdk-21_linux-x64_bin.deb
dpkg -i jdk-21_linux-x64_bin.deb
rm -f jdk-21_linux-x64_bin.deb
