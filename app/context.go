/*
Package app
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Application context.  Most of the values are set by the cli manager.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package app

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"io"
	"os"
)

// #####################################################################################################################
// # GINFRA CONTEXT
// # This is meant for the app and not services.

var (
	rootContext *GContext
)

func init() {
	rootContext, _ = NewContext(nil)
}

type GContext struct {
	Debugging bool
	Target    string
	Source    string

	InterfaceDir string
	NoControl    bool

	// Cmd related
	//Name string

	// Service related
	ServiceHomePath string
	ServicePath     string // Optional service path used for things like docker name.  It defaults to the service name.
	ServiceClass    string
	Deployment      string
	Port            int
	Specifics       string // json
	Clear           bool
	DontStart       bool // Don't actually start the service.  Useful for IDE testing, so the service is registered and etc.
	NoRollback      bool // Do not rollback anything in case of error.
	CommandLine     string
	Debugger        int //  Tell the target (container) to attach a debugger.  This value will be the listen port.  0 means don't do it.

	// Container related
	InitContainer string

	// Platform
	PortalHostnameUrl string // This could be host with docker engine or any entry point for managed platforms.

	logger *zap.SugaredLogger
	alog   zap.AtomicLevel

	// For testing
	MostRecentOutput string
	HostDirPath      string
}

// NewContext creates a new GContext with the specified alternate io.Writer for logging.
// If alt is nil, the logging output is directed only to os.Stdout.
// The function returns the created GContext and an error if any.
func NewContext(alt io.Writer) (*GContext, error) {
	gctx := &GContext{
		Debugging: false,
	}

	gctx.alog = zap.NewAtomicLevel()
	gctx.alog.SetLevel(zapcore.InfoLevel)

	//ecfg := zap.NewProductionEncoderConfig()
	ecfg := zap.NewDevelopmentEncoderConfig()

	var l *zap.Logger
	if alt == nil {
		l = zap.New(zapcore.NewCore(
			zapcore.NewJSONEncoder(ecfg),
			zapcore.Lock(os.Stdout),
			gctx.alog,
		))
	} else {
		core := zapcore.NewTee(
			zapcore.NewCore(zapcore.NewJSONEncoder(ecfg), zapcore.Lock(os.Stdout), gctx.alog),
			zapcore.NewCore(zapcore.NewJSONEncoder(ecfg), zapcore.Lock(zapcore.AddSync(alt)), gctx.alog),
		)
		l = zap.New(core)
	}

	defer func(l *zap.Logger) {
		_ = l.Sync()
	}(l)
	gctx.logger = l.Sugar()

	return gctx, nil
}

func GetRootContext() *GContext {
	return rootContext
}

func GetCopyRootContext() GContext {
	return *rootContext
}

func (gctx *GContext) TurnDebuggingOn() {
	gctx.Debugging = true
	gctx.alog.SetLevel(zapcore.DebugLevel)
}

func (gctx *GContext) GetLogger() *zap.SugaredLogger {
	if !gctx.Debugging {
		gctx.TurnDebuggingOn()
	}
	return gctx.logger
}

// #####################################################################################################################
// # GSERVICE CONTEXT
