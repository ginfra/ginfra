//go:build unit_test

/*
Package cmd
Copyright (c) 2024 Erich Gatejen
[LICENSE]

Test the commands.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package cmd

import (
	"bytes"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	testutil "gitlab.com/ginfra/ginfra/common/test"
	"os"
	"path/filepath"
	"testing"
)

func init() {
	common.SetupTest()
}

func TestRoot(t *testing.T) {
	c := NewRootCommand(nil)

	DebuggingFlag = c.PersistentFlags().Bool("debugging", false, "turn on debugging log")

	c.SetArgs([]string{"--debugging"})
	err := c.Execute()

	assert.NoError(t, err)
	assert.Equal(t, true, app.GetRootContext().Debugging)
}

func TestCmdRun(t *testing.T) {
	var buf bytes.Buffer
	var gctx, _ = app.NewContext(&buf)

	c := NewRunCommand(gctx)
	c.SetArgs([]string{filepath.Join(base.GetGinfraHome(), "app/run/test/1.yaml")})
	err := c.Execute()
	if err != nil {
		t.Errorf("Error executing run command: %v", err)
		return
	}

	sp, _ := testutil.NewScanPack(buf.String(), nil)
	assert.Greater(t, sp.Seek("Starting command"), 1)
	assert.Greater(t, sp.Seek("ginfra version"), 1)
	assert.Greater(t, sp.Seek("Scan successful"), 1)
	assert.Greater(t, sp.Seek("ginfra host"), 1)
}

func TestCmdRunPost(t *testing.T) {
	var buf bytes.Buffer
	var gctx, _ = app.NewContext(&buf)

	c := NewRunCommand(gctx)
	c.SetArgs([]string{filepath.Join(base.GetGinfraHome(), "app/cmd/test/1.yaml")})
	err := c.Execute()
	if err != nil {
		t.Errorf("Error executing run command: %v", err)
		return
	}

	sp, _ := testutil.NewScanPack(buf.String(), nil)
	assert.Greater(t, sp.Seek("Starting command"), 1)
	assert.Greater(t, sp.Seek("SCAN successful"), 1)
	assert.Equal(t, -1, sp.Seek("unknown flag: --bork"), "Post was not on.")
	assert.Greater(t, sp.Seek("unknown flag: --duck"), 1)
}

func TestCmdManage(t *testing.T) {
	gctx := app.GetCopyRootContext()
	gctx.ServiceHomePath = filepath.Join(base.GetGinfraHome(), "test", "local", "stub_service")

	c := NewLocalStart(&gctx)
	c.SilenceErrors = true
	c.SetArgs([]string{".", "extra", "junk"}) // This will fail, but that is ok.
	err := c.Execute()

	sp, _ := testutil.NewScanPack(err.Error(), nil)
	assert.Greater(t, sp.Seek("ssage"), 1)
	fmt.Printf("%v\n", err.Error())
	assert.Greater(t, sp.Seek("Unknown ConfigProvider type"), 1)
}

func TestCmdErrorNoError(t *testing.T) {
	gctx := app.GetCopyRootContext()
	gctx.ServiceHomePath = filepath.Join(base.GetGinfraHome(), "test", "local", "stub_service")

	cwd, err := os.Getwd()
	if err != nil {
		panic("OS could not get working directory.")
	}
	defer func(dir string) {
		_ = os.Chdir(dir)
	}(cwd)
	err = os.Chdir(gctx.ServiceHomePath)
	if err != nil {
		panic(err)
	}

	testCases := []struct {
		name    string
		cmd     *cobra.Command
		args    []string
		errText string
	}{
		{
			name:    "Tool snooze ok",
			cmd:     NewSnoozeCommand(),
			args:    []string{"10"},
			errText: "",
		},
		{
			name:    "Tool snooze bad value",
			cmd:     NewSnoozeCommand(),
			args:    []string{"sdf82jns9hg#$%"},
			errText: "Bad snooze value",
		},
		{
			name:    "Selenium run",
			cmd:     NewSeleniumRunCommand(),
			args:    []string{"afasdf", "asdfasdf", "asdfasdfa", "asdfasdf"},
			errText: "Could not load snippet",
		},
		{
			name:    "Selenium status",
			cmd:     NewSeleniumStatusCommand(),
			args:    []string{"afasdf"},
			errText: "Error contacting host",
		},
		{
			name:    "Selenium log",
			cmd:     NewSeleniumLogCommand(),
			args:    []string{"afasdf", "asdf"},
			errText: "Error contacting host",
		},
		{
			name:    "Selenium result",
			cmd:     NewSeleniumResultCommand(),
			args:    []string{"afasdf", "asdf"},
			errText: "Error contacting host",
		},
		{
			name:    "Selenium stop",
			cmd:     NewSeleniumStopCommand(),
			args:    []string{"afasdf", "asdfads"},
			errText: "Error contacting host",
		},
		{
			name:    "Ginterface merge",
			cmd:     NewGinterfaceMergeCmd(),
			args:    []string{"afasdf", "asdasd", "asdfasdf"},
			errText: "Unknown",
		},
		{
			name:    "Ginterface stubs",
			cmd:     NewGinterfaceStubsCmd(&gctx),
			args:    []string{"afas1239ehqihdf", "asdasd", "sdfasdf", "asdfasdf"},
			errText: "Unknown stub type",
		},
		{
			name:    "Root",
			cmd:     NewRootCommand(nil),
			args:    []string{},
			errText: "",
		},
		{
			name:    "Manage Host Init",
			cmd:     NewManageHostInitCmd(&gctx),
			args:    []string{},
			errText: "at least 1 arg",
		},
		{
			name:    "Manage Host Start",
			cmd:     NewManageHostStartCmd(&gctx),
			args:    []string{"aaa", "bbb"},
			errText: "Hosting.json",
		},
		{
			name:    "Manage Host List",
			cmd:     NewManageHostListCmd(&gctx),
			args:    []string{},
			errText: "Hosting.json",
		},
		{
			name:    "Manage Host Show",
			cmd:     NewManageHostShowCmd(&gctx),
			args:    []string{"aaaa"},
			errText: "Hosting.json",
		},
		{
			name:    "Manage Host Destroy",
			cmd:     NewManageHostDestroyCmd(&gctx),
			args:    []string{"aaaa"},
			errText: "Hosting.json",
		},
		{
			name:    "Manage Host Store",
			cmd:     NewManageHostStoreCmd(&gctx),
			args:    []string{"aaaa", "bbbb"},
			errText: "Hosting.json",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.cmd.SetArgs(tc.args) // This will fail, but that is ok.
			err := tc.cmd.Execute()
			if tc.errText == "" {
				assert.NoError(t, err)
			} else {
				assert.ErrorContains(t, err, tc.errText)
			}
		})
	}
}
