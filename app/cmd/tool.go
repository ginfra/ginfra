/*
Package cmd
Copyright (c) 2023 Erich Gatejen
[LICENSE]

# Run command

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/app/tool"
	"gitlab.com/ginfra/ginfra/base"
	"strconv"
	"time"
)

func init() {
	seleniumCmd.AddCommand(NewSeleniumRunCommand(), NewSeleniumStatusCommand(), NewSeleniumLogCommand(),
		NewSeleniumResultCommand(), NewSeleniumStopCommand())
	toolCmd.AddCommand(NewInfoCommand(), NewSnoozeCommand(), seleniumCmd)
	RootCmd.AddCommand(toolCmd)
}

var toolCmd = NewToolCommand()

func NewToolCommand() *cobra.Command {
	return &cobra.Command{
		Use:          "tool",
		Short:        "General tools",
		Long:         `General tools.`,
		SilenceUsage: true,
	}
}

// =====================================================================================================================
// == INFO

func NewInfoCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "info",
		Short: "General information.",
		Long:  `Get general information, which isn't much at this time.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.Info(app.GetRootContext())
		},
	}
}

// =====================================================================================================================
// == SNOOZE

func NewSnoozeCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "snooze [milliseconds]",
		Short: "Snooze for so many milliseconds.",
		Long:  `Snooze for so many milliseconds.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			t, err := strconv.Atoi(args[0])
			if err != nil {
				return base.NewGinfraErrorChild("Bad snooze value.", err)
			}
			time.Sleep(time.Duration(t) * time.Millisecond)
			return nil
		},
		Args: cobra.MinimumNArgs(1),
	}
}

// =====================================================================================================================
// == SELENIUM

var seleniumCmd = NewSeleniumCommand()

func NewSeleniumCommand() *cobra.Command {
	return &cobra.Command{
		Use:          "selenium",
		Short:        "Selenium host client.",
		Long:         `Selenium host client.`,
		SilenceUsage: true,
	}
}

func NewSeleniumRunCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "run [host] [target] [path to snippet]",
		Short: "Send snippet to selenium host.",
		Long:  `Send snippet to selenium host.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.SeleniumRun(app.GetRootContext(), args[0], args[1], args[2])
		},
		Args:         cobra.MinimumNArgs(3),
		SilenceUsage: true,
	}
}

func NewSeleniumStatusCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "status [host] ",
		Short: "Get status from selenium host.",
		Long:  `Get status from selenium host.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.SeleniumStatus(app.GetRootContext(), args[0])
		},
		Args:         cobra.MinimumNArgs(1),
		SilenceUsage: true,
	}
}

func NewSeleniumLogCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "log [host] [id]",
		Short: "Get log from selenium host for the given id.",
		Long:  `Get log from selenium host for the given id.  You can get the id from the status command.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.SeleniumLog(app.GetRootContext(), args[0], args[1])
		},
		Args:         cobra.MinimumNArgs(2),
		SilenceUsage: true,
	}
}

func NewSeleniumResultCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "result [host] [id]",
		Short: "Get test result from selenium host for the given id.",
		Long:  `Get test result  from selenium host for the given id.  You can get the id from the status command.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.SeleniumResult(app.GetRootContext(), args[0], args[1])
		},
		Args:         cobra.MinimumNArgs(2),
		SilenceUsage: true,
	}
}

func NewSeleniumStopCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "stop [host] [id] ",
		Short: "Get stop a snippet on the host.",
		Long:  `Get stop a snippet on the host.  The id is ignored in this version, since only one snippet may be running.`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return tool.SeleniumStop(app.GetRootContext(), args[0], args[1])
		},
		Args:         cobra.MinimumNArgs(2),
		SilenceUsage: true,
	}
}
