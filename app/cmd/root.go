/*
Package cmd
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Root cli command.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/common/config"
)

var (
	DebuggingFlag *bool
)

var RootCmd = NewRootCommand(app.GetRootContext())

func NewRootCommand(gctx *app.GContext) *cobra.Command {
	return &cobra.Command{
		Use:   "ginfra",
		Short: "GINFRA management tool",
		Long:  `GINFRA management tool`,
		PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
			return rootSetup()
		},
		Version: "main", // TODO set at build time.
	}
}

func Execute() error {
	return RootCmd.Execute()
}

func init() {
	DebuggingFlag = RootCmd.PersistentFlags().Bool("debugging", false, "turn on debugging log")
	RootCmd.PersistentFlags().StringVar(&app.GetRootContext().Target, "target",
		"", "output target directory or file path")
	RootCmd.PersistentFlags().StringVar(&app.GetRootContext().Deployment, config.CL_DEPLOYMENT,
		"default", "Deployment name.  'default' is the default.")
}

func rootSetup() error {
	if *DebuggingFlag {
		app.GetRootContext().TurnDebuggingOn()
	}

	return nil
}
