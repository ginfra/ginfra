/*
Package kube
Copyright (c) 2024 Erich Gatejen
[LICENSE] https://www.apache.org/licenses/LICENSE-2.0.txt

Manage kubernetes deployments.  Utilities for ginfra tasks.
*/
package kube

import (
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"os"
)

// #####################################################################################################################
// # SPECIFICS UTILS

func KubeStore(gctx *app.GContext, name, path, deployment, service string) (err error) {
	_, cprv, err := prepKube(gctx)
	if err != nil {
		return err
	}

	data, err := os.ReadFile(path)
	if err != nil {
		return base.NewGinfraErrorChildA("Could not read source file.", err, base.LM_FILEPATH, path)
	}

	return cprv.StorePut(name, deployment, service, data)
}
