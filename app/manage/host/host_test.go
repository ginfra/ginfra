//go:build host_test

/*
Package host
Copyright (c) 2023 Erich Gatejen
[LICENSE] https://www.apache.org/licenses/LICENSE-2.0.txt

Start host container tests.
*/
package host

import "testing"

func TestStartHostFunc(t *testing.T) {
	t.Run("Noop for now", func(t *testing.T) {

	})
}

func TestStartHostCases(t *testing.T) {
	t.Run("Noop for now", func(t *testing.T) {

	})
}
