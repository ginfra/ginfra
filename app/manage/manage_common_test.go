//go:build unit_test

/*
Package manage
Copyright (c) 2024 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt

Tests for common tools and utilities.
*/
package manage

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/common/config"
	ctest "gitlab.com/ginfra/ginfra/common/test"
	testutil "gitlab.com/ginfra/ginfra/common/test"
	"testing"
)

func TestManageSpecificsGood(t *testing.T) {
	gctx := app.GContext{
		Specifics: `{"spec1": "spec1value", "spec2": "spec2value"}`,
	}

	smap, err := GetServiceSpecifics(&gctx, []string{"spec3", "spec3value"})
	if err != nil {
		t.Error(err)
		return
	}
	ctest.TestCheckMapValue(t, false, smap, "spec1value", "spec1")
	ctest.TestCheckMapValue(t, false, smap, "spec2value", "spec2")
	ctest.TestCheckMapValue(t, false, smap, "spec3value", "spec3")
	ctest.TestCheckMapValue(t, true, smap, "spec3", "spec3value")
}

func TestManageSpecificsBad1(t *testing.T) {
	gctxbad1 := app.GContext{
		Specifics: `{"spec1": {, "spec2": "spec2value"}`,
	}

	_, err := GetServiceSpecifics(&gctxbad1, []string{"spec3", "spec3value"})
	assert.Error(t, err, "expecting broken specifics json")

	gctxbad2 := app.GContext{
		Specifics: `{"spec1": "asdfasdf", "spec2": "spec2value"}`,
	}
	_, err = GetServiceSpecifics(&gctxbad2, []string{"spec3", "spec3value", "unpaired"})
	assert.Error(t, err, "expecting broken specifics additional.")

}

func TestManageCases(t *testing.T) {
	var (
		err error
		cpv config.ConfigProvider
	)

	t.Run("Get config provider bad.", func(t *testing.T) {
		_, err = GetConfigProvider("xxx://yyy", "Q")
		assert.Error(t, err)
	})

	t.Run("Get config provider.", func(t *testing.T) {
		cpv, err = GetConfigProvider("memory://", config.AUTH_ADMIN_TOKEN_DEFAULT)
		if err != nil {
			t.Error(err)
			return
		}
		assert.NotNil(t, cpv)
	})

	t.Run("Rollback registration.", func(t *testing.T) {
		_, err = cpv.RegisterService(config.ITLocal, "test", "test", "test", "test",
			"memory://", "test", 8080, map[string]interface{}{})
		if err != nil {
			t.Error(err)
			return
		}

		var buf bytes.Buffer
		var gctx, _ = app.NewContext(&buf)

		RollbackRegistration(gctx, "test", "test", "test", cpv)
		sp, _ := testutil.NewScanPack(buf.String(), nil)
		assert.Greater(t, sp.Seek("Rolling back"), 1)
		assert.Equal(t, sp.Seek("Failed"), -1)
	})
	t.Run("Rollback registration fail.", func(t *testing.T) {
		var buf bytes.Buffer
		var gctx, _ = app.NewContext(&buf)

		RollbackRegistration(gctx, "xxx", "xxx", "xxx", cpv)
		sp, _ := testutil.NewScanPack(buf.String(), nil)
		assert.Greater(t, sp.Seek("iled to deregister"), 1)
	})
}
