/*
Package manage
Copyright (c) 2023 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt

Common tools and utilities.
*/
package manage

import (
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"gitlab.com/ginfra/ginfra/common/config"
	"go.uber.org/zap"
	"path"
)

// Hostfile

type HostFile struct {
	DeploymentName string `json:"DeploymentName"`

	ConfigAccessToken   string `json:"ConfigAccessToken"`
	ConfigUrl           string `json:"ConfigUrl"`
	ConfigPortalHostUrl string `json:"ConfigPortalHostUrl"`

	NextServicePort int `json:"NextServicePort"`

	// Host specific
	ConfigContainerId string `json:"ConfigContainerId"`
	DockerHost        string `json:"DockerHost"`
	NetworkName       string `json:"NetworkName"`
	NetworkId         string `json:"NetworkId"`

	// Kube specific
	InitConfigMapName string `json:"InitConfigMapName"`
}

func GetServiceSpecifics(gctx *app.GContext, additional []string) (map[string]interface{}, error) {
	var (
		err error
	)
	specifics := make(map[string]interface{}) // Can't be nil because the API will freak out.

	if gctx.Specifics != "" {
		if specifics, err = common.StringToJsonmap(gctx.Specifics); err != nil {
			return nil, base.NewGinfraErrorChildA("Bad value for specifics (--specifics json).", err, base.LM_TEXT,
				gctx.Specifics)
		}
	}
	if len(additional) > 0 {

		// Additional specifics.
		if additional != nil && len(additional) > 0 {

			if (len(additional) % 2) == 0 {
				for i := 0; i < len(additional); i = i + 2 {
					specifics[additional[i]] = additional[i+1]
				}
			} else {
				return nil, base.NewGinfraErrorA("Bad value for specifics (command line).  A name is not paired with a value.")
			}

		}
	}
	return specifics, err
}

func GetHostFilePath(gctx *app.GContext) string {
	return path.Join(gctx.HostDirPath, app.PathHostingFilePath)
}

func GetConfigProvider(url, accessToken string) (config.ConfigProvider, error) {
	var (
		prov config.ConfigProvider
		err  error
	)
	if prov, err = config.GetConfigProvider(url, accessToken); err != nil {
		err = base.NewGinfraErrorChildA("Could not load configuration provider", err, base.LM_CONFIG_PATH,
			url)
	}
	return prov, err
}

func RemoveRegistration(deployment, name string, cprv config.ConfigProvider) error {
	return cprv.DeregisterService(name, deployment)
}

func RollbackRegistration(gctx *app.GContext, deployment, name, stoken string, cprv config.ConfigProvider) {
	if stoken != "" {
		gctx.GetLogger().Info("Rolling back registration.")
		if err := RemoveRegistration(deployment, name, cprv); err != nil {
			gctx.GetLogger().Errorw("Failed to deregister service during rollback.",
				zap.String(base.LM_CAUSE, err.Error()))
		}
	}
}
