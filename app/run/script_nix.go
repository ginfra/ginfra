//go:build linux || darwin

/*
Package run
Copyright (c) 2023 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt

Nix only process decorator.

*/

package run

import (
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"go.uber.org/zap"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"syscall"
)

func DecorateCommand(gctx *app.GContext, c *exec.Cmd) {
	c.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
}

func TerminateCommand(gctx *app.GContext, c *exec.Cmd) {
	gctx.GetLogger().Infow("Killing command.", zap.String(base.LM_COMMAND, c.String()))

	if c != nil && c.Process != nil {
		pgid, err := syscall.Getpgid(c.Process.Pid)
		if err == nil {
			// Using the exec calls just aren't reliable on some linux distros.
			if err = syscall.Kill(-pgid, 2); err == nil {
				// err = c.Wait()
				// Using release instead of wait because there isn't anything to wait for.  Any pipe will be abandoned
				// anyway whereas when using Wait there is a chance of one ghosting and hanging the call.
				_ = c.Process.Release()

			} else {
				gctx.GetLogger().Error("Couldn't kill command", zap.String(base.LM_PID, strconv.Itoa(pgid)),
					zap.Error(err))

			}
		}
	}

}

func WaitKilled(gctx *app.GContext) {
	// Just wait for the term signal
	gctx.GetLogger().Infow("...sleepy time.")
	select {}
}

func SetupINTTrap(gctx *app.GContext, sctx *ScriptContext) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sig
		gctx.GetLogger().Info("INTERRUPTED.  Killing commands.")
		terminateDeferred(gctx, sctx)
		gctx.GetLogger().Info("Exiting.")
		os.Exit(0)
	}()
}
