//go:build windows

/*
Package run
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Nix only process decorator.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/

package run

import (
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"go.uber.org/zap"
	"os"
	"os/exec"
	"strconv"
)

func DecorateCommand(gctx *app.GContext, c *exec.Cmd) {
	// NOP
}

func TerminateCommand(gctx *app.GContext, c *exec.Cmd) {
	gctx.GetLogger().Infow("Killing command.", zap.String(base.LM_COMMAND, c.String()))

	// There is no easy way to do this, so just call the stupid command.
	kill := exec.Command("TASKKILL", "/T", "/F", "/PID", strconv.Itoa(c.Process.Pid))
	if e := kill.Run(); e != nil {
		gctx.GetLogger().Warn("May not have kill process Windows process.",
			zap.String(base.LM_PID, strconv.Itoa(c.Process.Pid)))
	}
}

func WaitKilled(gctx *app.GContext) {
	gctx.GetLogger().Infow("...sleepy time.")
	select {}
}

func SetupINTTrap(gctx *app.GContext, sctx *ScriptContext) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		gctx.GetLogger().Info("INTERRUPTED.  Killing commands.")
		terminateDeferred(gctx, sctx)
		gctx.GetLogger().Info("Exiting.")
		os.Exit(0)
	}()
}
