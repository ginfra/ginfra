# Interface Definitions

These are provided interface definitions.  You don't have to use them at all, but any API service we create will 
have their definitions here.  If you plan on using the configuration service, you will need to use 
'service_giconfig.json'  Note that not all tools do a good job handling $ref's from one file to another.  For this
reason, the 'ginfra merge' feature of the ginfra app can merge the specs into a single file.

