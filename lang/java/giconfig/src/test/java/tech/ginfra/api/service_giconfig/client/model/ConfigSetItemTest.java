/*
 * GIConfig Service
 * Configuration service.
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: inquiries@ginfra.tech
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package tech.ginfra.api.service_giconfig.client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Arrays;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Model tests for ConfigSetItem
 */
public class ConfigSetItemTest {
    private final ConfigSetItem model = new ConfigSetItem();

    /**
     * Model tests for ConfigSetItem
     */
    @Test
    public void testConfigSetItem() {
        // TODO: test ConfigSetItem
    }

    /**
     * Test the property 'path'
     */
    @Test
    public void pathTest() {
        // TODO: test path
    }

    /**
     * Test the property 'block'
     */
    @Test
    public void blockTest() {
        // TODO: test block
    }

}
