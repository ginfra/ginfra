/*
 * GIConfig Service
 * Configuration service.
 *
 * The version of the OpenAPI document: 0.0.1
 * Contact: inquiries@ginfra.tech
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package tech.ginfra.api.service_giconfig.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Arrays;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tech.ginfra.api.service_giconfig.client.invoker.JSON;

/**
 * ConfigPortalRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2024-09-08T16:38:58.410387983-06:00[America/Denver]")
public class ConfigPortalRequest {
  public static final String SERIALIZED_NAME_PORTAL = "portal";
  @SerializedName(SERIALIZED_NAME_PORTAL)
  private String portal;

  public static final String SERIALIZED_NAME_SCLASS = "sclass";
  @SerializedName(SERIALIZED_NAME_SCLASS)
  private String sclass;

  public ConfigPortalRequest() {
  }

  public ConfigPortalRequest portal(String portal) {
    this.portal = portal;
    return this;
  }

   /**
   * Typically an address or DN for the portal.
   * @return portal
  **/
  @javax.annotation.Nullable
  public String getPortal() {
    return portal;
  }

  public void setPortal(String portal) {
    this.portal = portal;
  }


  public ConfigPortalRequest sclass(String sclass) {
    this.sclass = sclass;
    return this;
  }

   /**
   * Service class.
   * @return sclass
  **/
  @javax.annotation.Nullable
  public String getSclass() {
    return sclass;
  }

  public void setSclass(String sclass) {
    this.sclass = sclass;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConfigPortalRequest configPortalRequest = (ConfigPortalRequest) o;
    return Objects.equals(this.portal, configPortalRequest.portal) &&
        Objects.equals(this.sclass, configPortalRequest.sclass);
  }

  @Override
  public int hashCode() {
    return Objects.hash(portal, sclass);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ConfigPortalRequest {\n");
    sb.append("    portal: ").append(toIndentedString(portal)).append("\n");
    sb.append("    sclass: ").append(toIndentedString(sclass)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }


  public static HashSet<String> openapiFields;
  public static HashSet<String> openapiRequiredFields;

  static {
    // a set of all properties/fields (JSON key names)
    openapiFields = new HashSet<String>();
    openapiFields.add("portal");
    openapiFields.add("sclass");

    // a set of required properties/fields (JSON key names)
    openapiRequiredFields = new HashSet<String>();
  }

 /**
  * Validates the JSON Element and throws an exception if issues found
  *
  * @param jsonElement JSON Element
  * @throws IOException if the JSON Element is invalid with respect to ConfigPortalRequest
  */
  public static void validateJsonElement(JsonElement jsonElement) throws IOException {
      if (jsonElement == null) {
        if (!ConfigPortalRequest.openapiRequiredFields.isEmpty()) { // has required fields but JSON element is null
          throw new IllegalArgumentException(String.format("The required field(s) %s in ConfigPortalRequest is not found in the empty JSON string", ConfigPortalRequest.openapiRequiredFields.toString()));
        }
      }

      Set<Map.Entry<String, JsonElement>> entries = jsonElement.getAsJsonObject().entrySet();
      // check to see if the JSON string contains additional fields
      for (Map.Entry<String, JsonElement> entry : entries) {
        if (!ConfigPortalRequest.openapiFields.contains(entry.getKey())) {
          throw new IllegalArgumentException(String.format("The field `%s` in the JSON string is not defined in the `ConfigPortalRequest` properties. JSON: %s", entry.getKey(), jsonElement.toString()));
        }
      }
        JsonObject jsonObj = jsonElement.getAsJsonObject();
      if ((jsonObj.get("portal") != null && !jsonObj.get("portal").isJsonNull()) && !jsonObj.get("portal").isJsonPrimitive()) {
        throw new IllegalArgumentException(String.format("Expected the field `portal` to be a primitive type in the JSON string but got `%s`", jsonObj.get("portal").toString()));
      }
      if ((jsonObj.get("sclass") != null && !jsonObj.get("sclass").isJsonNull()) && !jsonObj.get("sclass").isJsonPrimitive()) {
        throw new IllegalArgumentException(String.format("Expected the field `sclass` to be a primitive type in the JSON string but got `%s`", jsonObj.get("sclass").toString()));
      }
  }

  public static class CustomTypeAdapterFactory implements TypeAdapterFactory {
    @SuppressWarnings("unchecked")
    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
       if (!ConfigPortalRequest.class.isAssignableFrom(type.getRawType())) {
         return null; // this class only serializes 'ConfigPortalRequest' and its subtypes
       }
       final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);
       final TypeAdapter<ConfigPortalRequest> thisAdapter
                        = gson.getDelegateAdapter(this, TypeToken.get(ConfigPortalRequest.class));

       return (TypeAdapter<T>) new TypeAdapter<ConfigPortalRequest>() {
           @Override
           public void write(JsonWriter out, ConfigPortalRequest value) throws IOException {
             JsonObject obj = thisAdapter.toJsonTree(value).getAsJsonObject();
             elementAdapter.write(out, obj);
           }

           @Override
           public ConfigPortalRequest read(JsonReader in) throws IOException {
             JsonElement jsonElement = elementAdapter.read(in);
             validateJsonElement(jsonElement);
             return thisAdapter.fromJsonTree(jsonElement);
           }

       }.nullSafe();
    }
  }

 /**
  * Create an instance of ConfigPortalRequest given an JSON string
  *
  * @param jsonString JSON string
  * @return An instance of ConfigPortalRequest
  * @throws IOException if the JSON string is invalid with respect to ConfigPortalRequest
  */
  public static ConfigPortalRequest fromJson(String jsonString) throws IOException {
    return JSON.getGson().fromJson(jsonString, ConfigPortalRequest.class);
  }

 /**
  * Convert an instance of ConfigPortalRequest to an JSON string
  *
  * @return JSON string
  */
  public String toJson() {
    return JSON.getGson().toJson(this);
  }
}

