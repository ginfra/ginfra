# Notes

I have learned that Java 1.17 is the best version to compile and run this.  I'm at the mercy of the
openapi generator and it chooses the maven and gradle versions I have to use.  Depending on its version, 
Gradle has numerous conflicts with java versions.  I know 1.17 works with this setup.
