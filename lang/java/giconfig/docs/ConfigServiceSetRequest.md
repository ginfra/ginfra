

# ConfigServiceSetRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**failmode** | [**FailmodeEnum**](#FailmodeEnum) |  |  [optional] |
|**items** | [**List&lt;ConfigSetItem&gt;**](ConfigSetItem.md) |  |  [optional] |



## Enum: FailmodeEnum

| Name | Value |
|---- | -----|
| NONE | &quot;none&quot; |
| ROLLBACK | &quot;rollback&quot; |



