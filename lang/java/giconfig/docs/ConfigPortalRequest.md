

# ConfigPortalRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**portal** | **String** | Typically an address or DN for the portal. |  [optional] |
|**sclass** | **String** | Service class. |  [optional] |



