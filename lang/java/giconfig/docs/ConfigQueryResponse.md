

# ConfigQueryResponse


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**qpath** | **String** |  |  [optional] |
|**block** | **String** |  |  [optional] |



