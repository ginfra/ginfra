

# ConfigSetStateResponse


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**state** | **Integer** | State |  [optional] |



