

# DiscoverServiceResponse


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**uri** | **String** | Discovered uri |  [optional] |



