

# ConfigServiceRegistrationResponse


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**name** | **String** | System unique name |  [optional] |
|**confauth** | **String** | A non-OAuth authorization token. |  [optional] |



