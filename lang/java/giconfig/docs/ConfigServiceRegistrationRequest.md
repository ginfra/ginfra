

# ConfigServiceRegistrationRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**itype** | **String** | Instance host type |  [optional] |
|**name** | **String** | System unique name |  [optional] |
|**deployment** | **String** | Deployment name.  If blank it will be the default deployment. |  [optional] |
|**auth** | **String** | Service control authorization token. |  [optional] |
|**configprovider** | **String** | URI to configuration provider.  This is not authoritive. |  [optional] |
|**port** | **Integer** | Exposed service port |  [optional] |
|**image** | **String** |  |  [optional] |
|**serviceclass** | **String** |  |  [optional] |
|**specifics** | **String** |  |  [optional] |



