

# ConfigServiceInstanceRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**name** | **String** | System unique name |  [optional] |
|**deployment** | **String** | Deployment name.  If blank it will be the default deployment. |  [optional] |
|**sauth** | **String** | Service friendly config authorization token. |  [optional] |
|**itype** | **String** | Specific instance type. |  [optional] |
|**id** | **String** | IP address. |  [optional] |
|**ip** | **String** | IP address. |  [optional] |
|**fqdn** | **String** | FQDN for the entrypoint for the service. |  [optional] |



