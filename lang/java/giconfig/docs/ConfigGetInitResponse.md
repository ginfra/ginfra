

# ConfigGetInitResponse


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**state** | **Integer** | Discovered uri |  [optional] |



