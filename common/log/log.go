/*
Package log
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Logging facility.  I have decided to use go.uber.org/zap.  I have been a fan of structured logging for a long time and
have been adding it to my projects since 2002.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	"path/filepath"
)

const BaseFileName = "ginfra.log"

func GetLogger(homedir string, debugging bool) *zap.Logger {
	var level = zap.InfoLevel
	if debugging {
		level = zap.DebugLevel
	}
	l := zap.NewAtomicLevelAt(level)

	wrt := zapcore.AddSync(&lumberjack.Logger{
		Filename:   filepath.Join(homedir, BaseFileName),
		MaxSize:    75,
		MaxBackups: 10,
		MaxAge:     7,
	})

	ecfg := zap.NewDevelopmentEncoderConfig()
	ecfg.TimeKey = "timestamp"
	ecfg.EncodeTime = zapcore.ISO8601TimeEncoder

	e := zapcore.NewJSONEncoder(ecfg)
	return zap.New(zapcore.NewCore(e, wrt, l))

}
