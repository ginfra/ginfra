//go:build functional_test

/*
Package log
Copyright (c) 2024 Erich Gatejen
[LICENSE]

Logging facility test.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package log

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	test "gitlab.com/ginfra/ginfra/common/test"
	"os"
	"path/filepath"
	"testing"
)

func init() {
	common.SetupTest()
}

func TestLog(t *testing.T) {

	t.Run("Do logging", func(t *testing.T) {
		l := GetLogger(base.GetGinfraTmpTestDir(), true)
		if l == nil {
			t.Error("Nil logger")
			return
		}
		l.Debug("This is a debugging log entry.  aaaaaaaa bbbbbbb")
	})

	t.Run("Verify logging", func(t *testing.T) {
		s, err := os.ReadFile(filepath.Join(base.GetGinfraTmpTestDir(), BaseFileName))
		scan, err := test.NewScanPack(string(s), err)
		if err != nil {
			t.Error(err)
			return
		}

		assert.Greater(t, scan.Seek("debugging log entry"), 0)

	})
}
