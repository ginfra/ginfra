/*
Package shared
Copyright (c) 2024 Erich Gatejen
[LICENSE]

Shared infrastructure for api services.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package shared

import (
	"fmt"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/routers"
	"github.com/getkin/kin-openapi/routers/gorillamux"
	"github.com/labstack/echo/v4"
	echomiddleware "github.com/labstack/echo/v4/middleware"
	apiechomiddleware "github.com/oapi-codegen/echo-middleware"
	"gitlab.com/ginfra/ginfra/base"
)

// #####################################################################################################################
// # GENERAL

type SchemaValidator struct {
	r routers.Router
}

func GetNewSchemaValidator(swagger *openapi3.T) (*SchemaValidator, error) {
	var (
		sv  SchemaValidator
		err error
	)

	sv.r, err = gorillamux.NewRouter(swagger)
	if err != nil {
		// May unpanic this later.
		panic(err)
	}

	return &sv, err
}

func GetValidationSchemas(sw []*openapi3.T) ([]*SchemaValidator, error) {
	var (
		sv []*SchemaValidator
	)

	for _, i := range sw {
		if nsv, err := GetNewSchemaValidator(i); err == nil {
			sv = append(sv, nsv)
		} else {
			return nil, err
		}
	}

	return sv, nil
}

// #####################################################################################################################
// # ECHO MIDDLEWARE

func ValidatorManager(schemas []*SchemaValidator) echo.MiddlewareFunc {
	s := schemas // for debugging
	var skip echomiddleware.Skipper = echomiddleware.DefaultSkipper

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var err error

			// TODO HAX: Need a more generic way to register skips for validation.  For now, just ignore websocket connections.
			if c.Request().RequestURI != "/ws" {
				for _, i := range s {
					if !skip(c) {
						e := apiechomiddleware.ValidateRequestFromContext(c, i.r, nil)
						if e == nil {
							err = nil
							break
						}
						err = base.NewGinfraErrorA(e.Error(), base.LM_CODE, e.Code, base.LM_ERROR,
							fmt.Sprintf("%s", e.Message))
					}
				}

			}

			if err == nil {
				return next(c)
			}
			return err
		}
	}
}
