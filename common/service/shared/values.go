/*
Package shared
Copyright (c) 2024 Erich Gatejen
[LICENSE]

Shared values for services.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package shared

// =====================================================================================================================
// SERVICE CLASS RELATED

const ServiceClassOtelCollector = "server_otelcol/collector"
