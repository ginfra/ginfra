//go:build unit_test

/*
Package gotel
Copyright (c) 2024 Erich Gatejen
[LICENSE]

Test ifx tools.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package ifx

/*

type FakeClient struct {
	client.ClientWithResponses
	err error
	rsp http.Response
}

func (c *FakeClient) GiGetSpecificsWithResponse(ctx context.Context, reqEditors ...client.RequestEditorFn) (*client.GiGetSpecificsResponse,
	error) {
	return nil, nil
}

func (c *FakeClient) GiGetSpecifics(ctx context.Context, reqEditors ...client.RequestEditorFn) (*http.Response, error) {
	return &c.rsp, c.err
}

func TestPingService(t *testing.T) {

	var testCases = []struct {
		name string
		fc   FakeClient
		fail bool
		rt   int
	}{
		{
			name: "Good Ping",
			fc: FakeClient{
				rsp: http.Response{
					StatusCode: http.StatusOK,
					//Body:       io.NopCloser(strings.NewReader("{\"Specifics\": \"OK\"}")),
					Body:   io.NopCloser(strings.NewReader("\"OK\"")),
					Header: map[string][]string{"Content-Type": {"json"}},
				},
			},
			fail: false,
			rt:   1,
		},
		{
			name: "Bad Ping 500",
			fc: FakeClient{
				rsp: http.Response{
					StatusCode: http.StatusInternalServerError,
					Body:       io.NopCloser(strings.NewReader("{\"message\": \"bad message\", \"diagnostics\": \"bad diagnostics\"}")),
					Header:     map[string][]string{"Content-Type": {"json"}},
				},
			},
			fail: true,
			rt:   1,
		},
		{
			name: "Bad Ping 400",
			fc: FakeClient{
				rsp: http.Response{
					StatusCode: http.StatusBadRequest,
					Body:       io.NopCloser(strings.NewReader("")),
					Header:     map[string][]string{"Content-Type": {"json"}},
				},
			},
			fail: true,
			rt:   1,
		},
		{
			name: "Bad Ping 400 Retry",
			fc: FakeClient{
				rsp: http.Response{
					StatusCode: http.StatusBadRequest,
					Body:       io.NopCloser(strings.NewReader("")),
					Header:     map[string][]string{"Content-Type": {"json"}},
				},
			},
			fail: true,
			rt:   2,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			plc = &client.ClientWithResponses{ClientInterface: &tc.fc}
			pretries = 1

			_, err := PingService("asd", "asd", "Asd")
			if tc.fail {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}

}

*/
