/*
Package ifx
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Interface tools.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package ifx

import (
	"context"
	control "gitlab.com/ginfra/ginfra/api/service_control/client"
	"gitlab.com/ginfra/ginfra/app"
	"gitlab.com/ginfra/ginfra/base"
	"net/http"
	"time"
)

var (
	plc      *control.ClientWithResponses
	pretries = app.ServiceConnectRetries
)

func pingServiceClient(c *control.Client) error {
	hc := &http.Client{
		Timeout: time.Duration(app.ServiceConnectRetryTime) * time.Millisecond,
	}
	c.Client = hc
	return nil
}

func PingService(host, port, service, name string) (string, error) {
	var err error

	if plc == nil {
		plc, err = control.NewClientWithResponses("http://"+host+":"+port+"/"+service, pingServiceClient)
		if err != nil {
			return "", base.NewGinfraErrorA(name+" can not be contacted because client could not be created.", base.LM_CAUSE,
				err)
		}
	}
	defer func() { plc = nil }() // These should be on demand unless a test is posting it.

	var gr *control.GiGetSpecificsResponse

	for i := 0; i < pretries; i++ {

		gr, err = plc.GiGetSpecificsWithResponse(context.Background())
		if gr != nil && gr.StatusCode() == http.StatusOK {
			break
		}
		if i >= pretries-1 {
			// Tries are up.  Real problem.
			if gr != nil {
				return "", base.NewGinfraErrorA(name+" not available.", base.LM_CAUSE, string(gr.Body), base.LM_CODE,
					gr.StatusCode())
			} else {
				return "", base.NewGinfraErrorA(name+" not available.", base.LM_CAUSE, err)
			}

		} else {
			time.Sleep(time.Duration(app.ServiceConnectRetryTime*i) * time.Millisecond)
		}

	}
	return *gr.JSON200, nil
}
