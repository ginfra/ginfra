/*
Package shared
Copyright (c) 2024 Erich Gatejen
[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt

Service and tool configuration manipulation.
*/
package config

import (
	"fmt"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"strings"
)

// #####################################################################################################################
// # TYPES AND VALUES

type ProcessConfig struct {
	Deployment string
	Service    string
}

const ConfigPathPartSeparator = "."

const ConfigPathPrefix = "$"
const ConfigDiscoverServicePrefix = "="
const ConfigDiscoverPortalPrefix = "*"

const ConfigPointPrefix = "%"
const ConfigPointDeployment = "DEPLOYMENT"
const ConfigPointService = "SERVICE"

// #####################################################################################################################
// # Func

func prepareConfigPath(provider ConfigProvider, cfg *ProcessConfig, target string) ([]string, error) {
	var err error
	path := strings.Split(target, ConfigPathPartSeparator)
	for i, v := range path {
		if path[i], err = processConfigDirective(provider, cfg, v); err != nil {
			return []string{}, err
		}
	}
	return path, err
}

func processConfigDirective(provider ConfigProvider, cfg *ProcessConfig, target string) (string, error) {
	var (
		r   = target
		err error
	)

	if strings.HasPrefix(target, ConfigPointPrefix) {
		switch strings.ToUpper(target[1:]) {
		case ConfigPointDeployment:
			r = cfg.Deployment
		case ConfigPointService:
			r = cfg.Service
		default:
			err = base.NewGinfraErrorA("Unknown configuration point directive.", base.LM_NAME, target)
		}
	}

	return r, err
}

func processConfigPartPath(provider ConfigProvider, cfg *ProcessConfig, target string) (string, error) {
	var (
		r    string
		path []string
		err  error
	)

	if path, err = prepareConfigPath(provider, cfg, target); err != nil {
		return "", err
	}

	if len(path) > 0 {
		c := provider.FetchConfig(path...)
		if c.Err == nil {
			r = c.GetValue(path[len(path)-1]).GetValueStringNoerr()

		} else {
			err = c.Err
		}
	}

	return r, err
}

func processConfigPartDiscoverService(provider ConfigProvider, cfg *ProcessConfig, target string) (string, error) {
	var (
		r    string
		err  error
		path []string
	)

	if path, err = prepareConfigPath(provider, cfg, target); err != nil {
		return "", err
	}

	if len(path) == 2 {
		r, err = provider.DiscoverService(path[0], path[1])

	} else {
		err = base.NewGinfraErrorA("Poorly formed Discover Service point.")
	}

	return r, err
}

func processConfigPartDiscoverPortal(provider ConfigProvider, cfg *ProcessConfig, target string) (string, error) {
	var (
		r    string
		err  error
		path []string
	)

	if path, err = prepareConfigPath(provider, cfg, target); err != nil {
		return "", err
	}

	if len(path) == 2 {
		r, err = provider.DiscoverPortal(path[0], path[1])

	} else {
		err = base.NewGinfraErrorA("Poorly formed Discover Portal point.")
	}

	return r, err
}

func processConfigPart(provider ConfigProvider, cfg *ProcessConfig, target string) (string, error) {
	var (
		r   string
		err error
	)

	if strings.HasPrefix(target, ConfigPathPrefix) {
		r, err = processConfigPartPath(provider, cfg, target[1:])
	} else if strings.HasPrefix(target, ConfigDiscoverServicePrefix) {
		r, err = processConfigPartDiscoverService(provider, cfg, target[1:])
	} else if strings.HasPrefix(target, ConfigDiscoverPortalPrefix) {
		r, err = processConfigPartDiscoverPortal(provider, cfg, target[1:])
	} else {
		err = base.NewGinfraErrorAFlags("Unknown directive.", base.ERRFLAG_RECOVERABLE, base.LM_TARGET, target)
	}

	return r, err
}

func ProcessConfigString(provider ConfigProvider, config ProcessConfig, target string) (string, error) {

	var errs []error

	s, err := common.SimpleReplacerHandler(&target, func(in string) string {
		ss, e := processConfigPart(provider, &config, in)
		if e != nil {
			errs = append(errs, e)
		}
		return ss
	}, false)

	var r string
	if s != nil {
		r = *s
	}

	if err != nil {
		errs = append(errs, err)
	}

	// Let's see how this looks.
	if len(errs) > 0 {
		err = fmt.Errorf("errors occurred during processing: %v", errs)
	}

	return r, err
}

func ProcessVarConfigString(provider ConfigProvider, config ProcessConfig, v map[string]interface{}, target string) (string,
	error) {

	var errs []error

	s, err := common.SimpleReplacerHandler(&target, func(in string) string {
		ss, e := processConfigPart(provider, &config, in)
		if e != nil {
			if (base.GetErrorFlags(e) & base.ERRFLAG_RECOVERABLE) > 0 {
				if val, ok := v[in]; ok {
					ss = fmt.Sprintf("%v", val)
				}
			} else {
				errs = append(errs, e)
			}
		}
		return ss
	}, false)

	var r string
	if s != nil {
		r = *s
	}

	if err != nil {
		errs = append(errs, err)
	}

	// Let's see how this looks.
	if len(errs) > 0 {
		err = fmt.Errorf("errors occurred during processing: %v", errs)
	}

	return r, err
}
