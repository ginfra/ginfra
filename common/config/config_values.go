/*
Package config
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Configuration utilities as well as defined values.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package config

// #####################################################################################################################
// # DEFINED VALUES

const DEFAULT_DEPLOYMENT_NAME = "default"

const CONFIG_FILE_PREFIX = "config."

const CONFIG_FILE_STORE = "store"
const CONFIG_FILE_ALL = "all"
const CONFIG_FILE_DEPLOYMENT = "deployment"
const CONFIG_FILE_SERVICE = "service"

// #####################################################################################################################
// # SUGGESTED SERVICE CLASSES.   But you can use anything you want.

const SERVICE_CLASS__USERINFO = "userinfo"
