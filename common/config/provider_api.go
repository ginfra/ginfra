/*
Package config
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Configuration providers.  This is an API based provider that can us any service that implements the
ginterfaces/service_giconfig.yaml.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package config

import (
	"bytes"
	"context"
	"gitlab.com/ginfra/ginfra/api/service_giconfig/client"
	"gitlab.com/ginfra/ginfra/api/service_giconfig/models"
	"gitlab.com/ginfra/ginfra/base"
	"gitlab.com/ginfra/ginfra/common"
	"gitlab.com/ginfra/ginfra/common/ifx"
	"net/http"
)

// #####################################################################################################################
// # HTTP API CONFIG PROVIDER

type configProviderApi struct {
	Host, Port, Auth string
	Client           *client.ClientWithResponses
}

func GetConfigProviderApi(host, port, service, token string) (ConfigProvider, error) {

	var (
		prov configProviderApi
		err  error
	)
	prov.Host = host
	prov.Port = port
	prov.Auth = token

	// Don't use localhost or loopback.  They could end up on an interface that other nodes can't talk to.
	prov.Host = common.GetActualLocalhostIf(prov.Host)

	// Ping the service to check health.
	_, err = ifx.PingService(prov.Host, prov.Port, service, "Configuration provider")
	if err != nil {
		return nil, base.NewGinfraErrorA("Configuration provider could not contact the configuration service",
			base.LM_CAUSE, err)
	}

	// Create a client.  The only way to get an error is through the options, which we don't use now.
	prov.Client, err = client.NewClientWithResponses("http://" + host + ":" + port)
	if err != nil {
		return nil, base.NewGinfraErrorA("Configuration provider could not create a client", base.LM_CAUSE,
			err)
	}

	return &prov, err
}

func (cpf *configProviderApi) IsConfigurable() bool {
	return true
}

func (cpf *configProviderApi) SetConfigAccessAuth(auth *string) {
	cpf.Auth = *auth
}

func (cpf *configProviderApi) FetchConfig(path ...string) *Config {

	var (
		config *Config
		params models.GiConfigQueryConfigParams
	)

	if len(path) < 1 {
		params.Qpath = ""
	} else if len(path) == 1 {
		params.Qpath = path[0]
	} else {
		params.Qpath = common.Path2String(path...)
	}
	params.Confauth = cpf.Auth

	if r, err := cpf.Client.GiConfigQueryConfigWithResponse(context.Background(), &params); err == nil {
		if r.StatusCode() != http.StatusOK {
			if r.StatusCode() == http.StatusNotFound {
				config = NewConfig(nil, base.NewGinfraErrorAFlags("Could not fetch config.  Path not found",
					base.ERRFLAG_NOT_FOUND, base.LM_CAUSE, string(r.Body), base.LM_CODE, r.StatusCode()))
			} else {
				config = NewConfig(nil, base.NewGinfraErrorA("Could not fetch config.", base.LM_CAUSE, string(r.Body),
					base.LM_CODE, r.StatusCode()))
			}
		} else {
			config = NewConfig(common.Base64ToJsonMap(*r.JSON200.Block)) // How in the world is this compiling?
		}

	} else {
		config = NewConfig(nil, err)
	}

	return config
}

func (cpf *configProviderApi) PostConfig(config *Config, path ...string) error {
	var (
		params models.GiConfigSetConfigParams
		body   models.GiConfigSetConfigJSONRequestBody
		items  []models.ConfigSetItem
	)
	params.Confauth = cpf.Auth
	cpath := common.Path2String(path...)

	// Only supporting one item for now.
	block := common.JsonMapToBase64(config.Config)
	item := models.ConfigSetItem{
		Path:  &cpath,
		Block: &block,
	}
	items = append(items, item)

	body.Items = &items
	r, err := cpf.Client.GiConfigSetConfigWithResponse(context.Background(), &params, body)
	if err == nil && r.StatusCode() != http.StatusOK {
		err = base.NewGinfraErrorA("Could not post config.", base.LM_CAUSE, string(r.Body), base.LM_CODE,
			r.StatusCode())
	}
	return err
}

// RemoveConfig will remove a configuration point and all children.
func (cpf *configProviderApi) RemoveConfig(path ...string) error {
	var (
		params models.GiRemoveConfigParams
	)
	params.Confauth = cpf.Auth
	params.Qpath = common.Path2String(path...)
	r, err := cpf.Client.GiRemoveConfigWithResponse(context.Background(), &params)
	if err == nil && r.StatusCode() != http.StatusOK {

		err = base.NewGinfraErrorA("Could not remove config.", base.LM_CAUSE, string(r.Body), base.LM_CODE,
			r.StatusCode())
	}
	return err
}

func (cpf *configProviderApi) RegisterService(itype InstanceType, name, deployment, auth, image, configProvider,
	serviceClass string, port int, sspc map[string]interface{}) (string, error) {
	var (
		params models.GiConfigRegisterServiceParams
		body   models.GiConfigRegisterServiceJSONRequestBody
		its    string
		err    error
	)

	if its, err = GetStringFromInstanceType(itype); err != nil {
		return "", err
	}
	spc := common.JsonMapToBase64(sspc)

	// -- Default deployment --------------------------------------------------------------
	params.Confauth = cpf.Auth
	body.Itype = &its
	body.Name = &name
	body.Deployment = &deployment
	body.Auth = &auth
	body.Image = &image
	body.Configprovider = &configProvider
	p32 := int32(port)
	body.Port = &p32
	body.Serviceclass = &serviceClass
	body.Specifics = &spc
	r, err := cpf.Client.GiConfigRegisterServiceWithResponse(context.Background(), &params, body)
	if err != nil {
		return CONFIG_NIL_AUTH, base.NewGinfraErrorChildA("Could not connect to configuration provider service.", err,
			base.LM_SERVICE_NAME, name, base.LM_TARGET, configProvider)
	}
	if r.StatusCode() != 200 {
		return CONFIG_NIL_AUTH, base.NewGinfraErrorChildA("Could not get register service.", err,
			base.LM_SERVICE_NAME, name, base.LM_CODE, r.StatusCode(), base.LM_CAUSE, string(r.Body))
	}
	return *r.JSON200.Confauth, nil
}

func (cpf *configProviderApi) RegisterInstance(itype InstanceType, name, deployment, auth, id, ip,
	fqdn string) (string, error) {
	var (
		params models.GiConfigRegisterInstanceParams
		body   models.GiConfigRegisterInstanceJSONRequestBody
		its    string
		err    error
	)

	if its, err = GetStringFromInstanceType(itype); err != nil {
		return "", err
	}

	// -- Default deployment --------------------------------------------------------------
	params.Confauth = cpf.Auth
	body.Name = &name
	body.Deployment = &deployment
	body.Sauth = &auth
	body.Itype = &its
	body.Id = &id
	body.Ip = &ip
	body.Fqdn = &fqdn
	r, err := cpf.Client.GiConfigRegisterInstanceWithResponse(context.Background(), &params, body)
	if err != nil || r.StatusCode() != 200 {
		return CONFIG_NIL_AUTH, base.NewGinfraErrorChildA("Could not register service instance.", err,
			base.LM_SERVICE_NAME, name, base.LM_CODE, r.StatusCode(), base.LM_CAUSE, string(r.Body))
	}
	return *r.JSON200.Confauth, nil
}

func (cpf *configProviderApi) PortalAdd(deployment, serviceClass, location string) (string, error) {
	var (
		params models.GiConfigPortalParams
		body   models.GiConfigPortalJSONRequestBody
	)
	params.Confauth = cpf.Auth
	body.Sclass = &serviceClass
	body.Portal = &location

	r, err := cpf.Client.GiConfigPortalWithResponse(context.Background(), models.GiConfigPortalParamsOperation(OpPut),
		deployment, &params, body)
	if err != nil || r.StatusCode() != 200 {
		err = base.NewGinfraErrorChildA("Could not add portal.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass, base.LM_CAUSE, string(r.Body))
	}

	// Reserve the return for future use.
	return "", err
}

func (cpf *configProviderApi) PortalRemove(deployment, serviceClass string) error {
	var (
		params models.GiConfigPortalParams
		body   models.GiConfigPortalJSONRequestBody
	)
	params.Confauth = cpf.Auth
	body.Sclass = &serviceClass

	r, err := cpf.Client.GiConfigPortalWithResponse(context.Background(), models.GiConfigPortalParamsOperation(OpRemove),
		deployment, &params, body)
	if err != nil || r.StatusCode() != 200 {
		err = base.NewGinfraErrorChildA("Could not remove portal.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass, base.LM_CAUSE, string(r.Body))
	}

	// Reserve the return for future use.
	return err
}

func (cpf *configProviderApi) DeregisterService(name, deployment string) error {
	var (
		params models.GiConfigDeregisterServiceParams
	)
	params.Confauth = cpf.Auth

	r, err := cpf.Client.GiConfigDeregisterServiceWithResponse(context.Background(), deployment, name, &params)
	if err != nil || r.StatusCode() != 200 {
		return base.NewGinfraErrorChildA("Could not get deregister service deployment.", err,
			base.LM_SERVICE_NAME, name, base.LM_CODE, r.StatusCode(), base.LM_CAUSE, string(r.Body))
	}
	return nil
}

func (cpf *configProviderApi) DiscoveryAdd(deployment, serviceClass, uri string) error {
	var (
		params models.GiConfigDiscoveryAddParams
	)
	params.Confauth = cpf.Auth
	params.Deployment = deployment
	params.Sclass = serviceClass
	params.Uri = uri

	r, err := cpf.Client.GiConfigDiscoveryAddWithResponse(context.Background(), &params)
	if err != nil {
		return base.NewGinfraErrorChildA("Could not add discovery service class due to a problem.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass)
	}

	if r.StatusCode() != 200 {
		return base.NewGinfraErrorA("Could not add discovery service class because of a remote error.",
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass, base.LM_CODE, r.StatusCode(),
			base.LM_CAUSE, string(r.Body))
	}

	return nil
}

func (cpf *configProviderApi) DiscoverService(deployment, serviceClass string) (string, error) {
	var (
		params models.GiDiscoverServiceParams
	)
	params.Confauth = cpf.Auth

	r, err := cpf.Client.GiDiscoverServiceWithResponse(context.Background(), deployment, serviceClass, &params)
	if err != nil || r.StatusCode() != 200 {
		if r.StatusCode() == http.StatusNotFound {
			return "", base.NewGinfraErrorChildAFlags("Could not discover service class because none is running.",
				err, base.ERRFLAG_NOT_FOUND, base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass,
				base.LM_CODE, r.StatusCode(), base.LM_CAUSE, string(r.Body))
		} else {
			return "", base.NewGinfraErrorChildA("Could not discover service class due to a problem.", err,
				base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass, base.LM_CODE, r.StatusCode(),
				base.LM_CAUSE, string(r.Body))
		}
	}
	return *r.JSON200.Uri, nil
}

func (cpf *configProviderApi) DiscoverPortal(deployment, serviceClass string) (string, error) {
	var (
		params models.GiDiscoverPortalParams
	)
	params.Confauth = cpf.Auth

	r, err := cpf.Client.GiDiscoverPortalWithResponse(context.Background(), deployment, serviceClass, &params)
	if err != nil || r.StatusCode() != 200 {
		return "", base.NewGinfraErrorChildA("Could not discover portal for service class.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_CLASS, serviceClass, base.LM_CODE, r.StatusCode(),
			base.LM_CAUSE, string(r.Body))
	}
	return *r.JSON200.Uri, nil
}

func (cpf *configProviderApi) GetInitState(deployment, name, id string) (InitState, error) {
	var (
		params models.GiConfigGetInitParams
	)
	params.Confauth = cpf.Auth

	r, err := cpf.Client.GiConfigGetInitWithResponse(context.Background(), deployment, name, id, &params)
	if err != nil || r.StatusCode() != 200 {
		return 0, base.NewGinfraErrorChildA("Could not get init state for service.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_NAME, name, base.LM_CODE, r.StatusCode(),
			base.LM_CAUSE, string(r.Body))
	}
	return InitState(*r.JSON200.State), nil
}

func (cpf *configProviderApi) GetState(deployment, name string) (map[string]interface{}, error) {
	var (
		state  map[string]interface{}
		params models.GiConfigGetStateParams
	)
	params.Confauth = cpf.Auth

	if r, err := cpf.Client.GiConfigGetStateWithResponse(context.Background(), deployment, name, &params); err == nil {
		if r.StatusCode() == http.StatusOK {
			state, err = common.Base64ToJsonMap(*r.JSON200.Block) // How in the world is this compiling?

		} else {
			return nil, base.NewGinfraErrorA("Could not get state of service.", base.LM_DEPLOYMENT_NAME,
				deployment, base.LM_SERVICE_NAME, name, base.LM_CAUSE, string(r.Body), base.LM_CODE, r.StatusCode())
		}
	} else {
		return nil, base.NewGinfraErrorChildA("Could not get state from configuration service.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_NAME, name)
	}

	return state, nil
}

func (cpf *configProviderApi) PostState(deployment, name, id string, state map[string]interface{}) error {
	var (
		params models.GiConfigPostStateParams
		body   models.GiConfigPostStateJSONRequestBody
	)
	params.Confauth = cpf.Auth

	// Only supporting one item for now.
	block := common.JsonMapToBase64(state)
	body.Block = &block

	r, err := cpf.Client.GiConfigPostStateWithResponse(context.Background(), deployment, name, id, &params, body)
	if err == nil {
		if r.StatusCode() != http.StatusOK {
			err = base.NewGinfraErrorA("Could not post state.", base.LM_DEPLOYMENT_NAME, deployment,
				base.LM_SERVICE_NAME, name, base.LM_CAUSE, string(r.Body), base.LM_CODE, r.StatusCode())
		}
	} else {
		err = base.NewGinfraErrorChildA("Could not post state to configuration service.", err,
			base.LM_DEPLOYMENT_NAME, deployment, base.LM_SERVICE_NAME, name)
	}
	return err
}

func (cpf *configProviderApi) StorePut(name, deployment, service string, data []byte) error {
	var (
		params models.GiConfigStorePutParams
	)

	params.Confauth = cpf.Auth
	params.Name = name
	params.Deployment = &deployment
	params.Service = &service

	r, err := cpf.Client.GiConfigStorePutWithBodyWithResponse(context.Background(), &params,
		"application/octet-stream", bytes.NewReader(data))

	if err == nil {
		if r.StatusCode() != http.StatusOK {
			err = base.NewGinfraErrorA("Could not put file to store due to error.", base.LM_NAME, name,
				base.LM_CAUSE, string(r.Body), base.LM_CODE, r.StatusCode())
		}
	} else {
		err = base.NewGinfraErrorChildA("Could not put file to store.", err, base.LM_NAME, name)
	}
	return err
}

func (cpf *configProviderApi) StoreGet(name, deployment, service string, raw bool) ([]byte, error) {
	var (
		params models.GiConfigStoreGetParams
	)

	params.Confauth = cpf.Auth
	params.Name = name
	params.Deployment = &deployment
	params.Service = &service
	params.Raw = raw

	r, err := cpf.Client.GiConfigStoreGetWithResponse(context.Background(), &params)

	if err == nil {
		if r.StatusCode() != http.StatusOK {
			return []byte{}, base.NewGinfraErrorA("Could not get file from store due to error.", base.LM_NAME, name,
				base.LM_CAUSE, string(r.Body), base.LM_CODE, r.StatusCode())

		}

	} else {
		return []byte{}, base.NewGinfraErrorChildA("Could not get file from store.", err, base.LM_NAME, name)
	}

	return r.Body, nil

}

// ClearStore DO NOT implement this as service.  Consequently, the client is a NOOP.
func (cpf *configProviderApi) ClearStore() error {
	return nil
}
