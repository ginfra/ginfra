//go:build unit_test

/*
Package config
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Unit testing for config package.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package config

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ginfra/ginfra/base"
	"path/filepath"
	"testing"
)

// ===============================================================================================================
// =

const TCPSERVICE_NAME = "userservice"
const TCPSERVICE_DEPLOYMENT = "deployment1"
const TCPSERVICE_SERVICECLASS = SERVICE_CLASS__USERINFO

var (
	s1Name  = "user_provider"
	s1Value = "file://test/userfile.json"
	token1  string
)

func tcpRegisterService(p ConfigProvider) error {
	var err error
	sspc := make(map[string]interface{})
	sspc[s1Name] = s1Value
	if token1, err = p.RegisterService(ITLocal, TCPSERVICE_NAME, TCPSERVICE_DEPLOYMENT, "BBBBBBBBBBB", "image",
		"http://localhost:5111", TCPSERVICE_SERVICECLASS, 8900, sspc); err != nil {
		return err
	}
	_, err = p.RegisterInstance(ITLocal, TCPSERVICE_NAME, TCPSERVICE_DEPLOYMENT, token1, "aaa", "127.0.0.1",
		"localhost")
	return err
}

func tcpCheckRegistration(c *Config) error {
	cc := c.GetConfig(TCPSERVICE_DEPLOYMENT)
	if cc.Err != nil {
		return cc.Err
	}
	if cc = cc.GetConfigPath(TCPSERVICE_DEPLOYMENT, CRD_SERVICES, TCPSERVICE_NAME); cc.Err != nil {
		return cc.Err
	}

	cs := cc.GetConfig(CRD_SERVICES__SERVICE)
	if cs.Err != nil {
		return cs.Err
	}
	if cs.GetConfigPath(CRD_SSERVICE__CONFIG, CRD_SSERVICE__CONFIG__SPECIFICS).GetValue(s1Name).GetValueStringNoerr() !=
		s1Value {
		return errors.New("Specifics not found.")
	}
	if cs.GetConfigPath(CRD_SSERVICE__CONFIG).GetValue(CRD_SSERVICE__CONFIG__IMAGE).GetValueStringNoerr() !=
		"image" {
		return errors.New("Image not set.")
	}

	cs = cc.GetConfig(CRD_SERVICES__MANAGEMENT)
	if cs.Err != nil {
		return cs.Err
	}
	if cs.GetConfigPath(CRD_MSERVICE__INSTANCE, "aaa").GetValue(CRD_MSERVICE__INSTANCE__IP).GetValueStringNoerr() !=
		"127.0.0.1" {
		return errors.New("IP not set.")
	}
	return nil
}

func testHelpers(p ConfigProvider) error {
	c := GetServiceConfigN(p, TCPSERVICE_DEPLOYMENT, TCPSERVICE_NAME)
	if c.Err != nil {
		return c.Err
	}
	if c.GetValue(CRD_SSERVICE__CONFIG__CONFIG_PROVIDER).GetValueStringNoerr() != "http://localhost:5111" {
		return errors.New("Could not get config provider.")
	}

	_, c = GetServiceInstanceOnly(p, TCPSERVICE_DEPLOYMENT, TCPSERVICE_NAME)
	if c.Err != nil {
		return c.Err
	}
	if c.GetValue(CRD_MSERVICE__INSTANCE__TYPE).GetValueStringNoerr() != "local" {
		return errors.New("Could not get instance type.")
	}
	return nil
}

func tcpTestDiscovery(t *testing.T, rcp ConfigProvider) {
	err := rcp.DiscoveryAdd(TCPSERVICE_DEPLOYMENT, TCPSERVICE_SERVICECLASS, "http://localhost:8900")

	uri, err := rcp.DiscoverService(TCPSERVICE_DEPLOYMENT, TCPSERVICE_SERVICECLASS)
	assert.NoError(t, err, "Discover "+TCPSERVICE_SERVICECLASS)
	assert.Equal(t, "http://localhost:8900", uri, "Discover uri "+TCPSERVICE_SERVICECLASS)

	uri, err = rcp.DiscoverService(TCPSERVICE_DEPLOYMENT, "monkey")
	assert.Error(t, err, "Don't discover.")
}

func TestConfigProvider(t *testing.T) {
	p, _ := GetConfigProviderOnlyMemory()

	if err := tcpRegisterService(p); err != nil {
		t.Error(err)
		return
	}

	if err := tcpCheckRegistration(p.FetchConfig(CR_DEPLOYMENTS)); err != nil {
		t.Error(err)
		return
	}

	if err := testHelpers(p); err != nil {
		t.Error(err)
		return
	}

	tcpTestDiscovery(t, p)
}

func TestConfigProviderCases(t *testing.T) {

	t.Run("poorly formed provider url", func(t *testing.T) {
		assert.PanicsWithValue(t, "Poorly formed configuration provider uri: :ksdfsd:dsfsdf : parse \":ksdfsd:dsfsdf\": missing protocol scheme.  Note that uri's do not support windows drive letter notation.",
			func() { _, _ = GetConfigProvider(":ksdfsd:dsfsdf", "X") })
	})

	t.Run("poorly formed error proxy", func(t *testing.T) {
		_, err := GetConfigProvider("error:", "X")
		assert.ErrorContains(t, err, "No real provider uri")
		_, err = GetConfigProvider("error:asdfasfd", "X")
		assert.ErrorContains(t, err, "at least one directive")
		_, err = GetConfigProvider("error/sdfsdf/asdfasds:bork://", "X")
		assert.ErrorContains(t, err, "Unknown")
		_, err = GetConfigProvider("error/FetchConfig/:memory://", "X")
		assert.ErrorContains(t, err, "error directive")
	})

	t.Run("helpers", func(t *testing.T) {

		auth := AUTH_ADMIN_TOKEN_DEFAULT
		cp, err := GetConfigProviderFileSavable(filepath.Join(base.GetGinfraHome(), "common", "config", "test", "config.json"),
			0, true)
		if err != nil {
			t.Error(err)
			return
		}
		cp.SetConfigAccessAuth(&auth)

		// This should be the only time this package test will do this, but if not, it will panic here or somewhere else.
		base.RegisterService("giuser")
		c := GetServiceConfig(cp, "default")
		assert.NoError(t, c.Err)
		assert.Equal(t, "gservices/giuser", c.GetValue("image").GetValueStringNoerr())

		c = GetServiceInstance(cp, "default", "giuser", "709f9fd7cf56")
		assert.NoError(t, c.Err)
		assert.Equal(t, "host", c.GetValue("type").GetValueStringNoerr())

		cv := GetServiceAuthN(cp, "default", "giuser")
		assert.NoError(t, cv.GetValueError())
		assert.Equal(t, "MOMPT5A0GSLJIDPIENF5", cv.GetValueStringNoerr())

		// Not found
		cmp, err := GetConfigProviderOnlyMemory()
		assert.NoError(t, err)
		_, c = GetServiceInstanceOnly(cmp, "default", "giuser")
		assert.ErrorContains(t, c.Err, "No instances")
	})
}

func TestConfigProviderConfigProvider(t *testing.T) {
	t.Run("non-configurable functions allowed", func(t *testing.T) {
		c, err := GetConfigProviderConfigProvider()
		assert.NoError(t, err)
		assert.False(t, c.IsConfigurable())
		assert.NoError(t, c.ClearStore())

		v := "sfsdfs"
		c.SetConfigAccessAuth(&v)

		c, err = GetConfigProvider("config://", "XXXX")
		assert.NoError(t, err)
		assert.False(t, c.IsConfigurable())
		assert.Panics(t, func() { c.FetchConfig("blah") }, "FetchConfig did not panic.")
	})

	t.Run("panic when attempting configurable functions.", func(t *testing.T) {
		cpf := &configProviderConfigProvider{
			port: 8080,
		}
		assert.Panics(t, func() { cpf.FetchConfig("blah") }, "FetchConfig did not panic.")
		assert.Panics(t, func() { _ = cpf.PostConfig(&Config{}, "") }, "PostConfig did not panic.")
		assert.Panics(t, func() { _ = cpf.RemoveConfig("blah") }, "RemoveConfig did not panic.")
		assert.Panics(t, func() {
			_, _ = cpf.RegisterService(ITLocal, "blah", "blah", "blah",
				"blah", "blah", "blah", 8080, map[string]interface{}{})
		},
			"RegisterService did not panic.")
		assert.Panics(t, func() {
			_, _ = cpf.RegisterInstance(ITLocal, "name", "deployment", "auth",
				"id", "ip", "fqdn")
		}, "RegisterInstance did not panic.")
		assert.Panics(t, func() { _, _ = cpf.PortalAdd("", "", "") },
			"PortalAdd did not panic.")
		assert.Panics(t, func() { _ = cpf.PortalRemove("", "") }, "PortalRemove did not panic.")
		assert.Panics(t, func() { _ = cpf.DeregisterService("", "") }, "DeregisterService did not panic.")
		assert.Panics(t, func() { _ = cpf.DiscoveryAdd("", "", "") }, "DiscoveryAdd did not panic.")
		assert.Panics(t, func() { _, _ = cpf.DiscoverService("", "") },
			"DiscoverService did not panic.")
		assert.Panics(t, func() { _, _ = cpf.DiscoverPortal("", "") }, "DiscoverPortal did not panic.")
		assert.Panics(t, func() { _, _ = cpf.GetInitState("", "", "") }, "GetInitState did not panic.")
		assert.Panics(t, func() { _, _ = cpf.GetState("", "") }, "GetState did not panic.")
		assert.Panics(t, func() { _ = cpf.PostState("", "", "", map[string]interface{}{}) },
			"PostState did not panic.")
		assert.Panics(t, func() { _ = cpf.StorePut("", "", "", []byte{}) }, "StorePut did not panic.")
		assert.Panics(t, func() { _, _ = cpf.StoreGet("", "", "", true) }, "StoreGet did not panic.")
	})

}
