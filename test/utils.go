/*
Package test
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Various utilities.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package test
