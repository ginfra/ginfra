# Testing

## Unit Testing

TBD

## Functional Testing

TBD

## Local Testing



## Higher order testing.

IMPORTANT:  DO NOT put any tests here that participate in 'go test'.  They will be excluded from
coverage if you do.  Tests need to be with the code being tested.  You can put all the tools and 
helpers for those tests here.

Of course, tests that aren't run with 'go test' can go here.

## Next available port

The next available port available for the Minimal Configuration Server is 9502.  If you use that point, please update
this document.

