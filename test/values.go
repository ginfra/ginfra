/*
Package test
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Various values.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package test

// NAMES

// PATHS

const PathFlagFile = "flagfile.txt"

// ENVIRONMENTS

// SYSTEM

// SERVICE
