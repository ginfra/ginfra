/*
Package base
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Various tools and helpers.  They should all be obvious in what they do.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package base

import (
	"go/ast"
	"go/parser"
	"go/token"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func init() {
	rand.New(rand.NewSource(time.Now().UnixNano()))
}

func GetRandomFileName(suffix string) string {
	return strconv.Itoa(rand.Intn(428301510)) + "_" + suffix
}

const (
	passchars              = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+[].:"
	codechars              = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	alphachars             = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	COMMON_PASSWORD_LENGTH = 16
)

// GeneratePassword WARNING!  Randomness is broken.  I'll look into it before I release this version, but for now
// it is very handy for testing.
func GeneratePassword(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = passchars[rand.Intn(len(passchars))]
	}
	return string(b)
}

func GenerateCodeword(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = codechars[rand.Intn(len(codechars))]
	}
	return string(b)
}

func GenerateAlphaword(length int) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = alphachars[rand.Intn(len(alphachars))]
	}
	return string(b)
}

// Truth checks if a string value represents a truthy or falsy value.
// It returns true if the value is truthy and false otherwise.
// A value is considered truthy if it is not an empty string and not equal
// to "f", "F", "false", or "0" (case-insensitive).
// The comparison for falsy values is case-sensitive.
func Truth(v string) bool {
	if v == "" || v == "f" || v == "F" || strings.ToLower(v) == "false" || v == "0" {
		return false
	}
	return true
}

func SnipId(id string) string {
	if len(id) > 12 {
		return id[:ShortIdLength]
	}
	return id
}

func GetGoAST(targetFile string) (f *ast.File, err error) {
	fset := token.NewFileSet()
	return parser.ParseFile(fset, targetFile, nil, parser.AllErrors)
}
