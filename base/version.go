/*
Package base
Copyright (c) 2023 Erich Gatejen
[LICENSE] https://www.apache.org/licenses/LICENSE-2.0.txt

Version values.
*/
package base

var Version = "0.2.0"
