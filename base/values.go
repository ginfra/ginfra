/*
Package base
Copyright (c) 2023 Erich Gatejen
[LICENSE]

Values and helper functions to find common values use by the other packages.

[LICENSE]: https://www.apache.org/licenses/LICENSE-2.0.txt
*/
package base

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
)

// CONFIGURATION POINTS
// There are dependant scripts that are sensitive to any changes to these.  BE CAREFUL.
const (
	ConfigAuthLength     = 20
	ConfigFileName       = "service_config.json"
	ConfigMountDir       = "/config"
	DebuggerFlagFileName = "DEBUGGER"

	ConfigAuth        = "auth" // Authorization to manage service.
	ConfigUri         = "config_uri"
	ConfigPortalUri   = "config_portal_uri"
	ConfigAccessToken = "config_access_token" // Access token to config service.
	ConfigDeployment  = "deployment"
	ConfigName        = "name"
	ConfigType        = "type"
	ConfigPort        = "port"
	ConfigSpecifics   = "specifics"
	ConfigId          = "id" // Specific instance Id.

	ConfigClear = "clear"
)

// SPECIFICS

const SpecificsFileName = "specifics.json"

// GENERAL

const ShortIdLength = 12

// SERVICE
const (
	ConfigServiceName = "configservice"
)

// # ----------------------------------------------------------------------------------------------------------------
// # This is meant for the services, but other things can use it.

var (
	executableDirPath string
	registeredService string
)

func SetupValues() error {

	var (
		err      error = nil
		execPath string
	)

	// Get root directory
	if execPath, err = os.Executable(); err == nil {
		executableDirPath = filepath.Dir(execPath)
	}
	return err
}

// GetExecutableDirPath  Not sure that I want to keep this.  Be careful using it outside the services.  Also note that
// while your IDE may say it is not used, it could very well be used in the services that consume this module.
func GetExecutableDirPath() string {
	return executableDirPath
}

func RegisterService(name string) {
	if registeredService != "" {
		panic("Only one service can be registered per instance.")
	}
	registeredService = name
}

func GetRegisterService() string {
	return registeredService
}

// #####################################################################################################################
// # LOADERS AND CLIENTS FOR GENERALLY IMMUTABLE VALUES

const EnvGinfraHome = "GINFRA_HOME"
const EnvGinfraTestHome = "GINFRA_TEST_HOME"
const PathTmpDir = "tmp"
const PathTmpTestDir = "test"
const PathCacheDir = "cache"
const PathGinterfacesDir = "ginterfaces"
const PathServicesDir = "services"
const PathTemplatesDir = "_templates"

var (
	ginfraHomeDir = ""
)

// # ----------------------------------------------------------------------------------------------------------------
// # This is meant for the Ginfra executable, but others might have a use for it.

func LoadGinfra() error {
	var err error = nil

	if ginfraHomeDir == "" {
		// Normally using environment variables in code is a bad idea, but
		// this one has to be set for anything else to work.
		ginfraHomeDir = os.Getenv(EnvGinfraHome)
		if ginfraHomeDir == "" {
			err = errors.New(fmt.Sprintf("Required environment variable %s is not set.", EnvGinfraHome))
		}
	}

	return err
}

// GetGinfraHome gets the GINFRA_HOME path.  If you plan on using this or any below that use this, LoadGinfra() must
// be called first to initialize the value.  Anything under the application (/app) will have already done this during
// application startup.
func GetGinfraHome() string {
	return ginfraHomeDir
}

func GetGinfraTmpDir() string {
	return filepath.Join(GetGinfraHome(), PathTmpDir)
}

func GetGinfraRandomTmpDir() string {
	return filepath.Join(GetGinfraHome(), PathTmpDir,
		PathTmpDir+"_"+GenerateCodeword(10))
}

func GetGinfraTmpCacheDir() string {
	return filepath.Join(GetGinfraHome(), PathTmpDir, PathCacheDir)
}

func GetGinfraTmpTestDir() string {
	return filepath.Join(GetGinfraTmpDir(), PathTmpTestDir)
}

func GetGinfraGinterfacesDir() string {
	return filepath.Join(GetGinfraHome(), PathGinterfacesDir)
}

func GetGinfraTemplatesDir() string {
	return filepath.Join(GetGinfraHome(), PathServicesDir, PathTemplatesDir)
}

// SetGinfraHome is for testing.
func SetGinfraHome(p string) {
	ginfraHomeDir = p
}
