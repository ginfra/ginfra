## Why support windows?

The majority of modern cloud development is done on and for *nix, but this has been primarily 
done on windows.  My Macbook is watching me and weeping while I type this.  But why?
The folks I'm helping out with this are all using windows for development.  I need to make sure
it works first on windows and then in production *nix, which is where they will eventually
go live.  (I think I have finally talked them into using EKS, but we shall see.) 



