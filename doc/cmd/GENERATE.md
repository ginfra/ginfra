## GINFA COMMAND : GENERATE

### GENERATE SERVICE

ginfra generate service [Service name] [Service class] [Module Name] [flags]

### GENERATE SERVICE EXAMPLE

The following command generated the gservices/server_mongodb module.

```ginfra generate service server_mongodb server gitlab.com/ginfra/gservices/server_mongodb```

It was executed in the gservices/server_mongodb directory.  Note that



