# GINFRA APPLICATION COMMANDS AND USAGE

| Command                     | Purpose                            | 
|-----------------------------|------------------------------------|
| [generate](GENERATE.md)     | Generate services from templates.  |
| [tool](TOOL.md)             | Assorted tools .                   |
| [run](RUN.md)               | Run scripts orchestration.         |


